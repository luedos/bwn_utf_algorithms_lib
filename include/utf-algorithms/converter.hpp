#pragma once

#include "algorithms.hpp"

namespace utf
{

template<typename OutputCharT, typename InputCharT, EStrictness k_strictness = EStrictness::k_strict>
struct Converter
{
	using OutputAlgorithms = Algorithms<OutputCharT, k_strictness>;
	using InputAlgorithms = Algorithms<InputCharT, k_strictness>;

	template<typename OutputIteratorT, typename InputIteratorT>
	static bool convertCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool convertCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg);

	template<typename OutputIteratorT, typename InputIteratorT>
	static bool convertCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool convertCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg);

	// String coversion follows next rules. It returns false only if conversion failes. If we have ran out of input or output, it will still return true.
	// To check if you ran out of some iterator before whole string was converted, you can simply check input iterator to be equal end.
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool covertStringSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool covertStringUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);

	template<typename ContainerT, typename InputIteratorT>
	static ContainerT convertToContainer(InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	
	template<typename OutputContainerT, typename InputContainerT>
	static OutputContainerT convertToContainer(const InputContainerT& p_container);
};

template<typename CharT, EStrictness k_strictness>
struct Converter<CharT, CharT, k_strictness>
{
	using CharAlgorithms = Algorithms<CharT, k_strictness>;

	template<typename OutputIteratorT, typename InputIteratorT>
	static bool convertCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool convertCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg);

	template<typename OutputIteratorT, typename InputIteratorT>
	static bool convertCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool convertCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg);

	template<typename OutputIteratorT, typename InputIteratorT>
	static bool covertStringSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool covertStringUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);

	template<typename ContainerT, typename InputIteratorT>
	static ContainerT convertToContainer(InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);

	template<typename OutputContainerT, typename InputContainerT>
	static OutputContainerT convertToContainer(const InputContainerT& p_container);
};

#if ((defined(_MSVC_LANG) && _MSVC_LANG >= 201402L) || __cplusplus >= 201402L)
    #define UTF_ALGORITHMS_CONSTEXPR constexpr
#else
	#define UTF_ALGORITHMS_CONSTEXPR
#endif

// If strict is true, the algorithms will skip invalid bytes, otherwise they will return k_unicodeCharStub
template<typename CharT, EStrictness k_strictness = EStrictness::k_strict>
class IterativeConverter;

template<EStrictness k_strictness>
class IterativeConverter<Char8, k_strictness>
{
	//
	// Construction and destruction.
	//
public:
	UTF_ALGORITHMS_CONSTEXPR IterativeConverter() = default;

	//
	// Public interface.
	//
public:
	// If char forms complete utf8 sequence, returns true, and writes parsed sequence into o_output.
	// If pushed char didn't complete the utf8 sequence returns false.
	// In case if new sequence was started when old was yet to complete, the old sequence will be discarded.
	inline UTF_ALGORITHMS_CONSTEXPR bool push(const Char8 _char, char32_t& o_output);
	// Clears internal cache of the converter.
	inline UTF_ALGORITHMS_CONSTEXPR void clear();

	//
	// Private members.
	//
private:
	uint32_t m_returnCode = 0;
	uint8_t m_trailingBytesLeft = 0;
	uint8_t m_trailingBytes = 0;
};

template<EStrictness k_strictness>
class IterativeConverter<char16_t, k_strictness>
{
	//
	// Construction and destruction.
	//
public:
	UTF_ALGORITHMS_CONSTEXPR IterativeConverter() = default;

	//
	// Public interface.
	//
public:
	// If char forms complete utf8 sequence, returns true, and writes parsed sequence into o_output.
	// If pushed char didn't complete the utf8 sequence returns false.
	// In case if new sequence was started when old was yet to complete, the old sequence will be discarded.
	inline UTF_ALGORITHMS_CONSTEXPR bool push(const char16_t _char, char32_t& o_output);
	// Clears internal cache of the converter.
	inline UTF_ALGORITHMS_CONSTEXPR void clear();

	//
	// Private members.
	//
private:
	char16_t m_first = 0;
};

template<EStrictness k_strictness>
class IterativeConverter<char32_t, k_strictness>
{
	//
	// Construction and destruction.
	//
public:
	UTF_ALGORITHMS_CONSTEXPR IterativeConverter() = default;

	//
	// Public interface.
	//
public:
	// The most simplest version out of all converters. Simply returns provided value.
	inline UTF_ALGORITHMS_CONSTEXPR bool push(const char32_t _char, char32_t& o_output) { o_output = _char; return true; }
	inline UTF_ALGORITHMS_CONSTEXPR void clear() {}
};

template<EStrictness k_strictness>
class IterativeConverter<wchar_t, k_strictness> : private IterativeConverter<typename detail::UtfCharTypeHelper<wchar_t>::Type, k_strictness>
{
	using UnderlyingChar = typename detail::UtfCharTypeHelper<wchar_t>::Type;

	//
	// Construction and destruction.
	//
public:
	UTF_ALGORITHMS_CONSTEXPR IterativeConverter() = default;

	//
	// Public interface.
	//
public:
	inline UTF_ALGORITHMS_CONSTEXPR bool push(const wchar_t _char, char32_t& o_output)
	{
		return this->IterativeConverter<UnderlyingChar, k_strictness>::push(static_cast<UnderlyingChar>(_char), o_output);
	}
	using IterativeConverter<UnderlyingChar, k_strictness>::clear;
};

} // namespace utf

#undef UTF_ALGORITHMS_CONSTEXPR

#include "converter.inl"