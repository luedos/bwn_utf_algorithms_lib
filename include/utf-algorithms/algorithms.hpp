#pragma once
// heavily inspired by https://github.com/Alexhuszagh/UTFPP
#include <cstdint>
#include <cstddef>
#include <type_traits>

namespace utf
{

using Char8 = char;

namespace detail
{

static constexpr uint32_t k_unicodeCharStub = 0x0000FFFD;

template<typename CharT, typename=void>
struct UtfCharTypeHelper;

template<typename CharT>
struct UtfCharTypeHelper<CharT, typename std::enable_if<sizeof(CharT) == sizeof(Char8)>::type>
{
	using Type = Char8;
};

template<typename CharT>
struct UtfCharTypeHelper<CharT, typename std::enable_if<sizeof(CharT) == sizeof(char16_t)>::type>
{
	using Type = char16_t;
};

template<typename CharT>
struct UtfCharTypeHelper<CharT, typename std::enable_if<sizeof(CharT) == sizeof(char32_t)>::type>
{
	using Type = char32_t;
};

} // namespace detail

enum EStrictness
{
	// Strict algorithms will usually return false as soon as they will come across invalid sequence.
	k_strict,
	// Relaxed algorithms will go as far as possible. If invalid sequence is appears it usually replaced with k_unicodeCharStub
	k_relaxed
};

template<typename, EStrictness k_strictness = k_strict>
struct Algorithms;

template<EStrictness k_strictness>
struct Algorithms<Char8, k_strictness>
{
	// Maximum count of the bytes in sequence which could be requiered by the algorithms.
	static constexpr uint8_t k_maxByteCount = 6;

	static inline constexpr uint8_t getExtraBytesFromHeader(uint8_t p_headerByte);
	static inline constexpr bool isHeaderByte(uint8_t p_byte);
	static inline constexpr bool isTrailingByte(uint8_t p_byte);
	static inline constexpr bool isAsciiByte(uint8_t p_byte);

	template<typename IteratorT>
	static bool fromUnicodeSafe(IteratorT& p_outputBeg, IteratorT p_outputEnd, uint32_t p_code);	
	template<typename IteratorT>
	static bool fromUnicodeUnsafe(IteratorT& p_outputBeg, uint32_t p_code);
	
	template<typename IteratorT>
	static bool toUnicodeSafe(uint32_t& o_code, IteratorT& p_utf8Beg, IteratorT p_utf8End);	
	template<typename IteratorT>
	static bool toUnicodeUnsafe(uint32_t& o_code, IteratorT& p_utf8Beg);
	
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg);
	
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg);

	template<typename IteratorT>
	static bool nextSafe(IteratorT& p_beg, IteratorT p_end);	
	template<typename IteratorT>
	static bool nextUnsafe(IteratorT& p_beg);

	template<typename IteratorT>
	static size_t distance(IteratorT& p_beg, IteratorT p_end);
};

template<EStrictness k_strictness>
struct Algorithms<char16_t, k_strictness>
{
	// Maximum count of the bytes in sequence which could be requiered by the algorithms.
	static constexpr uint8_t k_maxByteCount = 2;

	template<typename IteratorT>
	static bool fromUnicodeSafe(IteratorT& p_outputBeg, IteratorT p_outputEnd, uint32_t p_code);	
	template<typename IteratorT>
	static bool fromUnicodeUnsafe(IteratorT& p_outputBeg, uint32_t p_code);
	
	template<typename IteratorT>
	static bool toUnicodeSafe(uint32_t& o_code, IteratorT& p_utf16Beg, IteratorT p_utf16End);	
	template<typename IteratorT>
	static bool toUnicodeUnsafe(uint32_t& o_code, IteratorT& p_utf16Beg);
	
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg);
	
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg);
	
	template<typename IteratorT>
	static bool nextSafe(IteratorT& p_beg, IteratorT p_end);	
	template<typename IteratorT>
	static bool nextUnsafe(IteratorT& p_beg);

	template<typename IteratorT>
	static size_t distance(IteratorT& p_beg, IteratorT p_end);
};

template<EStrictness k_strictness>
struct Algorithms<char32_t, k_strictness>
{
	// Maximum count of the bytes in sequence which could be requiered by the algorithms.
	static constexpr uint8_t k_maxByteCount = 1;

	template<typename IteratorT>
	static bool fromUnicodeSafe(IteratorT& p_outputBeg, IteratorT p_outputEnd, uint32_t p_code);
	template<typename IteratorT>
	static bool fromUnicodeUnsafe(IteratorT& p_outputBeg, uint32_t p_code);

	template<typename IteratorT>
	static bool toUnicodeSafe(uint32_t& o_code, IteratorT& p_utf32Beg, IteratorT p_utf32End);
	template<typename IteratorT>
	static bool toUnicodeUnsafe(uint32_t& o_code, IteratorT& p_utf32Beg);
	
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg);
	
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg);

	template<typename IteratorT>
	static bool nextSafe(IteratorT& p_beg, IteratorT p_end);
	template<typename IteratorT>
	static bool nextUnsafe(IteratorT& p_beg);

	template<typename IteratorT>
	static auto distance(IteratorT& p_beg, IteratorT p_end) ->
		typename std::enable_if<
			std::is_convertible<decltype(p_end - p_beg), size_t>::value,
			size_t
		>::type;
	template<typename IteratorT>
	static auto distance(IteratorT& p_beg, IteratorT p_end) ->
		typename std::enable_if<
			!std::is_convertible<decltype(p_end - p_beg), size_t>::value,
			size_t
		>::type;
};

template<EStrictness k_strictness>
struct Algorithms<wchar_t, k_strictness>
{
	using AliasAlgorithms = Algorithms<detail::UtfCharTypeHelper<wchar_t>::Type, k_strictness>;
	static_assert(
		!std::is_same<AliasAlgorithms, Algorithms<wchar_t, k_strictness>>::value, 
		"AliasAlgorithms is the algorithms representing which actual utf standart will be used for wchar_t, "
		"it can't be the same as Algorithms<wchar_t> or we will hit recursion.");

	// Maximum count of the bytes in sequence which could be requiered by the algorithms.
	static constexpr uint8_t k_maxByteCount = AliasAlgorithms::k_maxByteCount;

	template<typename IteratorT>
	static bool fromUnicodeSafe(IteratorT& p_outputBeg, IteratorT p_outputEnd, uint32_t p_code);
	template<typename IteratorT>
	static bool fromUnicodeUnsafe(IteratorT& p_outputBeg, uint32_t p_code);

	template<typename IteratorT>
	static bool toUnicodeSafe(uint32_t& o_code, IteratorT& p_wcharBeg, IteratorT p_wcharEnd);
	template<typename IteratorT>
	static bool toUnicodeUnsafe(uint32_t& o_code, IteratorT& p_wcharBeg);
	
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharSafe(OutputIteratorT& p_outputBeg, OutputIteratorT p_outputEnd, InputIteratorT& p_inputBeg);
	
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg, InputIteratorT p_inputEnd);
	template<typename OutputIteratorT, typename InputIteratorT>
	static bool copyCharUnsafe(OutputIteratorT& p_outputBeg, InputIteratorT& p_inputBeg);

	template<typename IteratorT>
	static bool nextSafe(IteratorT& p_beg, IteratorT p_end);
	template<typename IteratorT>
	static bool nextUnsafe(IteratorT& p_beg);

	template<typename IteratorT>
	static size_t distance(IteratorT& p_beg, IteratorT p_end);
};

}

#include "algorithms.inl"
#include "algorithmsInstantiations.hpp"