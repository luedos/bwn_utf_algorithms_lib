#pragma once

#include "algorithms.hpp"

#include <iterator>

/////////////////////////// char8 ///////////////////////////

template<utf::EStrictness k_strictness>
inline constexpr uint8_t utf::Algorithms<utf::Char8, k_strictness>::getExtraBytesFromHeader(uint8_t p_headerByte)
{
	return uint8_t((p_headerByte & 0xC0 ) == 0xC0 ) /* 0b11000000 */
		+ uint8_t((p_headerByte & 0xE0 ) == 0xE0 ) /* 0b11100000 */
		+ uint8_t((p_headerByte & 0xF0 ) == 0xF0 ) /* 0b11110000 */
		+ uint8_t((p_headerByte & 0xF8 ) == 0xF8 ) /* 0b11111000 */
		+ uint8_t((p_headerByte & 0xFC ) == 0xFC ); /* 0b11111100 */
}

template<utf::EStrictness k_strictness>
inline constexpr bool utf::Algorithms<utf::Char8, k_strictness>::isHeaderByte(uint8_t p_byte)
{
	return ((p_byte & 0xC0) == 0xC0 /* 0b11000000 */);
}

template<utf::EStrictness k_strictness>
inline constexpr bool utf::Algorithms<utf::Char8, k_strictness>::isTrailingByte(uint8_t p_byte)
{
	return (p_byte & 0xC0 /* 0b11000000 */) == 0x80 /* 0b10000000 */;
}

template<utf::EStrictness k_strictness>
inline constexpr bool utf::Algorithms<utf::Char8, k_strictness>::isAsciiByte(uint8_t p_byte)
{
	return (p_byte & 0x80 /* 0b10000000 */) == 0;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<utf::Char8, k_strictness>::fromUnicodeSafe(IteratorT& p_outputBeg, IteratorT p_outputEnd, uint32_t p_code)
{
	if (p_outputBeg >= p_outputEnd)
	{
		return false;
	}

	constexpr uint32_t k_maxUtf32 = 0x0010FFFF;
	constexpr uint32_t k_bytemark = 0x80;
	constexpr uint32_t k_bytemask = 0xBF;
	constexpr uint8_t k_firstExtraByteMark[6] = { 0x00, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC };

	short extraBytes;
	if (p_code > k_maxUtf32)
	{
		if (k_strictness == EStrictness::k_strict)
		{
			return false;
		}
		else
		{
			p_code = detail::k_unicodeCharStub;
			extraBytes = 2;
		}
	}
	else
	{
		extraBytes = 
			(p_code >= 0x80) 
			+ (p_code >= 0x800)
			+ (p_code >= 0x10000);
	}

	uint64_t shiftedCode = static_cast<uint64_t>(p_code) << (32 - 6 * extraBytes);
	IteratorT outputIt = p_outputBeg;

	// write to buffer
	*outputIt = static_cast<Char8>((shiftedCode >> 32) | k_firstExtraByteMark[extraBytes]);
	++outputIt;

	while (extraBytes != 0)
	{
		if (outputIt == p_outputEnd)
		{
			return false;
		}

		shiftedCode <<= 6;
		*outputIt = static_cast<Char8>(((shiftedCode >> 32) | k_bytemark) & k_bytemask);
		++outputIt;
		--extraBytes;
	}

	p_outputBeg = outputIt;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<utf::Char8, k_strictness>::fromUnicodeUnsafe(IteratorT& p_outputBeg, uint32_t p_code)
{
	constexpr uint32_t k_maxUtf32 = 0x0010FFFF;
	constexpr uint32_t k_bytemark = 0x80;
	constexpr uint32_t k_bytemask = 0xBF;
	constexpr uint8_t k_firstExtraByteMark[6] = { 0x00, 0xC0, 0xE0, 0xF0, 0xF8, 0xFC };

	short extraBytes;
	if (p_code > k_maxUtf32)
	{
		if (k_strictness == EStrictness::k_strict)
		{
			return false;
		}
		else
		{
			p_code = detail::k_unicodeCharStub;
			extraBytes = 2;
		}
	}
	else
	{
		extraBytes = 
			(p_code >= 0x80) 
			+ (p_code >= 0x800)
			+ (p_code >= 0x10000);
	}

	uint64_t shiftedCode = static_cast<uint64_t>(p_code) << (32 - 6 * extraBytes);

	// write to buffer
	*p_outputBeg = static_cast<Char8>((shiftedCode >> 32) | k_firstExtraByteMark[extraBytes]);
	++p_outputBeg;

	while (extraBytes != 0)
	{
		shiftedCode <<= 6;
		*p_outputBeg = static_cast<Char8>(((shiftedCode >> 32) | k_bytemark) & k_bytemask);
		++p_outputBeg;
		--extraBytes;
	}

	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<utf::Char8, k_strictness>::toUnicodeSafe(uint32_t& o_code, IteratorT& p_utf8Beg, IteratorT p_utf8End)
{
	if (p_utf8Beg >= p_utf8End)
	{
		return false;
	}

	IteratorT tempUtf8Beg = p_utf8Beg;

	const uint8_t headerByte = static_cast<uint8_t>(*tempUtf8Beg++);
	const uint8_t extraBytes = getExtraBytesFromHeader(headerByte);

	if (extraBytes > 0 && tempUtf8Beg == p_utf8End)
	{
		return false;
	}

	// Header byte
	uint32_t retCode = headerByte;

	for (uint8_t extraByteId = 0; extraByteId < extraBytes; ++extraByteId)
	{
		if (tempUtf8Beg == p_utf8End)
		{
			if (k_strictness == EStrictness::k_strict)
			{
				return false;
			}
			else
			{
				o_code = detail::k_unicodeCharStub;
				p_utf8Beg = tempUtf8Beg;
				return true;
			}
		}

		const uint8_t byte = static_cast<uint8_t>(*tempUtf8Beg);
		if (!isTrailingByte(byte))
		{
			if (k_strictness == EStrictness::k_strict)
			{
				return false;
			}
			else
			{
				o_code = detail::k_unicodeCharStub;
				p_utf8Beg = tempUtf8Beg;
				return true;
			}
		}
		retCode <<= 6;
		retCode += byte;
		++tempUtf8Beg;
	};

	constexpr uint32_t k_utf8Offsets[6] = { 0x00000000UL, 0x00003080UL, 0x000E2080UL, 0x03C82080UL, 0xFA082080UL, 0x82082080UL };
	retCode -= k_utf8Offsets[extraBytes];

	o_code = retCode;
	p_utf8Beg = tempUtf8Beg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<utf::Char8, k_strictness>::toUnicodeUnsafe(uint32_t& o_code, IteratorT& p_utf8Beg)
{
	IteratorT tempUtf8Beg = p_utf8Beg;

	const uint8_t headerByte = static_cast<uint8_t>(*tempUtf8Beg++);
	const uint8_t extraBytes = getExtraBytesFromHeader(headerByte);

	uint32_t retCode = headerByte;

	for (uint8_t extraByteId = 0; extraByteId < extraBytes; ++extraByteId)
	{
		const uint8_t byte = static_cast<uint8_t>(*tempUtf8Beg);
		if (!isTrailingByte(byte))
		{
			if (k_strictness == EStrictness::k_strict)
			{
				return false;
			}
			else
			{
				o_code = detail::k_unicodeCharStub;
				p_utf8Beg = tempUtf8Beg;
				return true;
			}
		}
		retCode <<= 6;
		retCode += byte;
		++tempUtf8Beg;
	};

	constexpr uint32_t k_utf8Offsets[6] = { 0x00000000UL, 0x00003080UL, 0x000E2080UL, 0x03C82080UL, 0xFA082080UL, 0x82082080UL };
	retCode -= k_utf8Offsets[extraBytes];

	o_code = retCode;
	p_utf8Beg = tempUtf8Beg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<utf::Char8, k_strictness>::copyCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	if (p_inputBeg >= p_inputEnd || p_outputBeg >= p_outputEnd)
	{
		return false;
	}

	InputIteratorT tempInputIt = p_inputBeg;
	OutputIteratorT tempOutputIt = p_outputBeg;

	const uint8_t firstByte = static_cast<uint8_t>(*tempInputIt++);
	const size_t extraBytes = getExtraBytesFromHeader(firstByte);

	uint8_t buffer[8];
	uint8_t* bufferIt = buffer;
	*bufferIt++ = firstByte;

	size_t count = 0;
	for(; tempInputIt != p_inputEnd && count < extraBytes; ++tempInputIt)
	{
		const uint8_t byte = static_cast<uint8_t>(*tempInputIt);
		if (!isTrailingByte(byte))
		{
			break;
		}

		*bufferIt++ = byte;
		++count;
	}

	if (count != extraBytes)
	{
		if (k_strictness == EStrictness::k_strict)
		{
			return false;
		}

		const bool result = Algorithms<utf::Char8, EStrictness::k_relaxed>::fromUnicodeSafe(p_outputBeg, p_outputEnd, detail::k_unicodeCharStub);
		if (result)
		{
			p_inputBeg = tempInputIt;
		}

		return result;
	}

	for (const uint8_t* bufferReadIt = buffer; bufferReadIt != bufferIt; ++bufferReadIt)
	{
		if (tempOutputIt == p_outputEnd)
		{
			return false;
		}

		*tempOutputIt++ = *bufferReadIt;
	}

	p_inputBeg = tempInputIt;
	p_outputBeg = tempOutputIt;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<utf::Char8, k_strictness>::copyCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg)
{
	if (p_outputBeg >= p_outputEnd)
	{
		return false;
	}

	InputIteratorT tempInputIt = p_inputBeg;
	OutputIteratorT tempOutputIt = p_outputBeg;

	const uint8_t firstByte = static_cast<uint8_t>(*tempInputIt++);
	const size_t extraBytes = getExtraBytesFromHeader(firstByte);

	uint8_t buffer[8];
	uint8_t* bufferIt = buffer;
	*bufferIt++ = firstByte;

	size_t count = 0;
	for(; count < extraBytes; ++tempInputIt)
	{
		const uint8_t byte = static_cast<uint8_t>(*tempInputIt);
		if (!isTrailingByte(byte))
		{
			break;
		}

		*bufferIt++ = byte;
		++count;
	}

	if (count != extraBytes)
	{
		if (k_strictness == EStrictness::k_strict)
		{
			return false;
		}

		const bool result = Algorithms<utf::Char8, EStrictness::k_relaxed>::fromUnicodeSafe(p_outputBeg, p_outputEnd, detail::k_unicodeCharStub);
		if (result)
		{
			p_inputBeg = tempInputIt;
		}

		return result;
	}

	for (const uint8_t* bufferReadIt = buffer; bufferReadIt != bufferIt; ++bufferReadIt)
	{
		if (tempOutputIt == p_outputEnd)
		{
			return false;
		}

		*tempOutputIt++ = *bufferReadIt;
	}

	p_inputBeg = tempInputIt;
	p_outputBeg = tempOutputIt;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<utf::Char8, k_strictness>::copyCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	if (p_inputBeg >= p_inputEnd)
	{
		return false;
	}

	InputIteratorT tempInputIt = p_inputBeg;
	OutputIteratorT tempOutputIt = p_outputBeg;

	const uint8_t firstByte = static_cast<uint8_t>(*tempInputIt++);
	const size_t extraBytes = getExtraBytesFromHeader(firstByte);

	uint8_t buffer[8];
	uint8_t* bufferIt = buffer;
	*bufferIt++ = firstByte;

	size_t count = 0;
	for(; tempInputIt != p_inputEnd && count < extraBytes; ++tempInputIt)
	{
		const uint8_t byte = static_cast<uint8_t>(*tempInputIt);
		if (!isTrailingByte(byte))
		{
			break;
		}

		*bufferIt++ = byte;
		++count;
	}

	if (count != extraBytes)
	{
		if (k_strictness == EStrictness::k_strict)
		{
			return false;
		}

		const bool result = Algorithms<utf::Char8, EStrictness::k_relaxed>::fromUnicodeUnsafe(p_outputBeg, detail::k_unicodeCharStub);
		if (result)
		{
			p_inputBeg = tempInputIt;
		}

		return result;
	}

	for (const uint8_t* bufferReadIt = buffer; bufferReadIt != bufferIt; ++bufferReadIt)
	{
		*tempOutputIt++ = *bufferReadIt;
	}

	p_inputBeg = tempInputIt;
	p_outputBeg = tempOutputIt;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<utf::Char8, k_strictness>::copyCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg)
{
	InputIteratorT tempInputIt = p_inputBeg;
	OutputIteratorT tempOutputIt = p_outputBeg;

	const uint8_t firstByte = static_cast<uint8_t>(*tempInputIt++);
	const size_t extraBytes = getExtraBytesFromHeader(firstByte);

	uint8_t buffer[8];
	uint8_t* bufferIt = buffer;
	*bufferIt++ = firstByte;

	size_t count = 0;
	for(; count < extraBytes; ++tempInputIt)
	{
		const uint8_t byte = static_cast<uint8_t>(*tempInputIt);
		if (!isTrailingByte(byte))
		{
			break;
		}

		*bufferIt++ = byte;
		++count;
	}

	if (count != extraBytes)
	{
		if (k_strictness == EStrictness::k_strict)
		{
			return false;
		}

		const bool result = Algorithms<utf::Char8, EStrictness::k_relaxed>::fromUnicodeUnsafe(p_outputBeg, detail::k_unicodeCharStub);
		if (result)
		{
			p_inputBeg = tempInputIt;
		}

		return result;
	}

	for (const uint8_t* bufferReadIt = buffer; bufferReadIt != bufferIt; ++bufferReadIt)
	{
		*tempOutputIt++ = *bufferReadIt;
	}

	p_inputBeg = tempInputIt;
	p_outputBeg = tempOutputIt;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<utf::Char8, k_strictness>::nextSafe(IteratorT& p_beg, IteratorT p_end)
{
	if (p_beg >= p_end)
	{
		return false;
	}

	const uint8_t extraBytes = getExtraBytesFromHeader(static_cast<uint8_t>(*p_beg));

	uint8_t count = 0; 
	
	IteratorT it = p_beg;
	++it;

	for(; it != p_end && (k_strictness == EStrictness::k_relaxed || count < extraBytes) && isTrailingByte(*it); ++it)
	{
		++count;
	}

	// Count should be equal to extra bytes.
	if (k_strictness == EStrictness::k_strict && count != extraBytes)
	{
		return false;
	}

	p_beg = it;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<utf::Char8, k_strictness>::nextUnsafe(IteratorT& p_beg)
{
	const uint8_t extraBytes = getExtraBytesFromHeader(static_cast<uint8_t>(*p_beg));

	uint8_t count = 0; 
	
	IteratorT it = p_beg;
	++it;

	for(; (k_strictness == EStrictness::k_relaxed || count < extraBytes) && isTrailingByte(*it); ++it)
	{
		++count;
	}

	// Count should be equal to extra bytes.
	if (k_strictness == EStrictness::k_strict && count != extraBytes)
	{
		return false;
	}

	p_beg = it;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
size_t utf::Algorithms<utf::Char8, k_strictness>::distance(IteratorT& p_beg, IteratorT p_end)
{
	size_t count = 0;
	while (nextSafe(p_beg, p_end))
	{
		++count;
	}

	return count;
}

/////////////////////////// char16_t ///////////////////////////

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char16_t, k_strictness>::fromUnicodeSafe(IteratorT& p_outputBeg, IteratorT p_outputEnd, uint32_t p_code)
{
	if (p_outputBeg >= p_outputEnd)
	{
		return false;
	}

	// limits
	constexpr uint32_t k_maxUtf32 = 0x0010FFFF;
	constexpr uint32_t k_highBegin = 0xD800;
	constexpr uint32_t k_lowBegin = 0xDC00;
	constexpr uint32_t k_maxbmp = 0x0000FFFF;
	constexpr int k_shift = 10;
	constexpr uint32_t k_base = 0x0010000UL;
	constexpr uint32_t k_mask = 0x3FFUL;

	IteratorT tempOutputBeg = p_outputBeg;

	// variables
	if (p_code <= k_maxbmp) 
	{
		if (p_code >= k_highBegin && p_code <= k_lowBegin) 
		{
			if (k_strictness == EStrictness::k_strict)
			{
				return false;
			}
			*tempOutputBeg++ = static_cast<char16_t>(detail::k_unicodeCharStub);
		} 
		else 
		{
			*tempOutputBeg++ = static_cast<char16_t>(p_code);
		}
	} 
	else if (p_code > k_maxUtf32) 
	{
		if (k_strictness == EStrictness::k_strict)
		{
			return false;
		}
		*tempOutputBeg++ = static_cast<char16_t>(detail::k_unicodeCharStub);
	} 
	else 
	{
		p_code -= k_base;
		*tempOutputBeg++ = static_cast<char16_t>((p_code >> k_shift) + k_highBegin);
		if (tempOutputBeg == p_outputEnd)
		{
			return false;
		}

		*tempOutputBeg++ = static_cast<char16_t>((p_code & k_mask) + k_lowBegin);
	}

	// Assigning all output parameters in the end.
	p_outputBeg = tempOutputBeg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char16_t, k_strictness>::fromUnicodeUnsafe(IteratorT& p_outputBeg, uint32_t p_code)
{
	// limits
	constexpr uint32_t k_maxUtf32 = 0x0010FFFF;
	constexpr uint32_t k_highBegin = 0xD800;
	constexpr uint32_t k_lowBegin = 0xDC00;
	constexpr uint32_t k_maxbmp = 0x0000FFFF;
	constexpr int k_shift = 10;
	constexpr uint32_t k_base = 0x0010000UL;
	constexpr uint32_t k_mask = 0x3FFUL;

	IteratorT tempOutputBeg = p_outputBeg;

	// variables
	if (p_code <= k_maxbmp) 
	{
		if (p_code >= k_highBegin && p_code <= k_lowBegin) 
		{
			if (k_strictness == EStrictness::k_strict)
			{
				return false;
			}
			*tempOutputBeg++ = static_cast<char16_t>(detail::k_unicodeCharStub);
		} 
		else 
		{
			*tempOutputBeg++ = static_cast<char16_t>(p_code);
		}
	} 
	else if (p_code > k_maxUtf32) 
	{
		if (k_strictness == EStrictness::k_strict)
		{
			return false;
		}
		*tempOutputBeg++ = static_cast<char16_t>(detail::k_unicodeCharStub);
	} 
	else 
	{
		p_code -= k_base;
		*tempOutputBeg++ = static_cast<char16_t>((p_code >> k_shift) + k_highBegin);
		*tempOutputBeg++ = static_cast<char16_t>((p_code & k_mask) + k_lowBegin);
	}

	// Assigning all output parameters in the end.
	p_outputBeg = tempOutputBeg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char16_t, k_strictness>::toUnicodeSafe(uint32_t& o_code, IteratorT& p_utf16Beg, IteratorT p_utf16End)
{
	if (p_utf16Beg >= p_utf16End)
	{
		return false;
	}

	// limits
	constexpr uint32_t k_highBegin = 0xD800;
	constexpr uint32_t k_highEnd = 0xDBFF;
	constexpr uint32_t k_lowBegin = 0xDC00;
	constexpr uint32_t k_lowEnd = 0xDFFF;
	constexpr int k_shift = 10;
	constexpr uint32_t k_base = 0x0010000UL;

	IteratorT tempUtf16Beg = p_utf16Beg;

	const uint32_t firstChar = *tempUtf16Beg++;
	if (firstChar >= k_highBegin && firstChar <= k_highEnd) 
	{		
		if (tempUtf16Beg == p_utf16End)
		{
			return false;
		}

		// surrogate pairs
		const uint32_t secondChar = *tempUtf16Beg;
		if (secondChar >= k_lowBegin && secondChar <= k_lowEnd) 
		{
			o_code = ((firstChar - k_highBegin) << k_shift) + (secondChar - k_lowBegin) + k_base;
			++tempUtf16Beg;
		}
		else if (k_strictness == EStrictness::k_relaxed)
		{
			o_code = detail::k_unicodeCharStub;
		}
		else
		{
			return false;
		}
	} 
	else if (firstChar >= k_lowBegin && firstChar <= k_lowEnd) 
	{
		if (k_strictness == EStrictness::k_strict)
		{
			return false;
		}
		o_code = detail::k_unicodeCharStub;
	} 
	else 
	{
		o_code = firstChar;
	}

	// We are returning early, only if something went wrong,
	// in any other case, we go through if/else block.
	p_utf16Beg = tempUtf16Beg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char16_t, k_strictness>::toUnicodeUnsafe(uint32_t& o_code, IteratorT& p_utf16Beg)
{
	// limits
	constexpr uint32_t k_highBegin = 0xD800;
	constexpr uint32_t k_highEnd = 0xDBFF;
	constexpr uint32_t k_lowBegin = 0xDC00;
	constexpr uint32_t k_lowEnd = 0xDFFF;
	constexpr int k_shift = 10;
	constexpr uint32_t k_base = 0x0010000UL;

	IteratorT tempUtf16Beg = p_utf16Beg;

	const uint32_t firstChar = *tempUtf16Beg++;
	if (firstChar >= k_highBegin && firstChar <= k_highEnd) 
	{
		// surrogate pairs
		const uint32_t secondChar = *tempUtf16Beg;
		if (secondChar >= k_lowBegin && secondChar <= k_lowEnd) 
		{
			o_code = ((firstChar - k_highBegin) << k_shift) + (secondChar - k_lowBegin) + k_base;
			++tempUtf16Beg;
		}
		else if (k_strictness == EStrictness::k_relaxed)
		{
			o_code = detail::k_unicodeCharStub;
		}
		else
		{
			return false;
		}
	} 
	else if (firstChar >= k_lowBegin && firstChar <= k_lowEnd) 
	{
		if (k_strictness == EStrictness::k_strict)
		{
			return false;
		}
		o_code = detail::k_unicodeCharStub;
	} 
	else 
	{
		o_code = firstChar;
	}

	// We are returning early, only if something went wrong,
	// in any other case, we go through if/else block.
	p_utf16Beg = tempUtf16Beg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<char16_t, k_strictness>::copyCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	if (p_outputBeg >= p_outputEnd || p_inputBeg >= p_inputEnd)
	{
		return false;
	}

	// limits
	constexpr uint32_t k_highBegin = 0xD800;
	constexpr uint32_t k_highEnd = 0xDBFF;
	constexpr uint32_t k_lowBegin = 0xDC00;
	constexpr uint32_t k_lowEnd = 0xDFFF;

	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	const uint32_t firstChar = *tempInputBeg++;
	if (firstChar >= k_highBegin && firstChar <= k_highEnd) 
	{
		// surrogate pairs
		if (tempInputBeg == p_inputEnd)
		{
			return false;
		}
		
		const uint32_t secondChar = *tempInputBeg;
		if (secondChar < k_lowBegin || secondChar > k_lowEnd)
		{
			if (k_strictness == EStrictness::k_strict)
			{
				return false;
			}

			const bool result = Algorithms<char16_t, utf::k_relaxed>::fromUnicodeSafe(p_outputBeg, p_outputEnd, detail::k_unicodeCharStub);
			if (result)
			{
				p_inputBeg = tempInputBeg;
			}

			return result;
		}

		++tempInputBeg;
		*tempOutputBeg++ = firstChar;
		if (tempOutputBeg == p_outputEnd)
		{
			return false;
		}

		*tempOutputBeg++ = secondChar;

		p_inputBeg = tempInputBeg;
		p_outputBeg = tempOutputBeg;
		return true;
	} 
	else if (k_strictness == EStrictness::k_strict && firstChar >= k_lowBegin && firstChar <= k_lowEnd) 
	{
		return false;
	} 
	else 
	{
		*p_outputBeg++ = firstChar;
		p_inputBeg = tempInputBeg;
		return true;
	}
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<char16_t, k_strictness>::copyCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg)
{
	if (p_outputBeg >= p_outputEnd)
	{
		return false;
	}

	// limits
	constexpr uint32_t k_highBegin = 0xD800;
	constexpr uint32_t k_highEnd = 0xDBFF;
	constexpr uint32_t k_lowBegin = 0xDC00;
	constexpr uint32_t k_lowEnd = 0xDFFF;

	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	const uint32_t firstChar = *tempInputBeg++;
	if (firstChar >= k_highBegin && firstChar <= k_highEnd)
	{
		// surrogate pairs
		const uint32_t secondChar = *tempInputBeg;
		if (secondChar < k_lowBegin || secondChar > k_lowEnd)
		{
			if (k_strictness == EStrictness::k_strict)
			{
				return false;
			}

			const bool result = Algorithms<char16_t, utf::k_relaxed>::fromUnicodeSafe(p_outputBeg, p_outputEnd, detail::k_unicodeCharStub);
			if (result)
			{
				p_inputBeg = tempInputBeg;
			}

			return result;
		}

		++tempInputBeg;
		*tempOutputBeg++ = firstChar;
		if (tempOutputBeg == p_outputEnd)
		{
			return false;
		}

		*tempOutputBeg++ = secondChar;

		p_inputBeg = tempInputBeg;
		p_outputBeg = tempOutputBeg;
		return true;
	}
	else if (k_strictness == EStrictness::k_strict && firstChar >= k_lowBegin && firstChar <= k_lowEnd)
	{
		return false;
	}
	else
	{
		*p_outputBeg++ = firstChar;
		p_inputBeg = tempInputBeg;
		return true;
	}
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<char16_t, k_strictness>::copyCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	if (p_inputBeg >= p_inputEnd)
	{
		return false;
	}

	// limits
	constexpr uint32_t k_highBegin = 0xD800;
	constexpr uint32_t k_highEnd = 0xDBFF;
	constexpr uint32_t k_lowBegin = 0xDC00;
	constexpr uint32_t k_lowEnd = 0xDFFF;

	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	const uint32_t firstChar = *tempInputBeg++;
	if (firstChar >= k_highBegin && firstChar <= k_highEnd)
	{
		// surrogate pairs
		if (tempInputBeg == p_inputEnd)
		{
			return false;
		}

		const uint32_t secondChar = *tempInputBeg;
		if (secondChar < k_lowBegin || secondChar > k_lowEnd)
		{
			if (k_strictness == EStrictness::k_strict)
			{
				return false;
			}

			const bool result = Algorithms<char16_t, utf::k_relaxed>::fromUnicodeUnsafe(p_outputBeg, detail::k_unicodeCharStub);
			if (result)
			{
				p_inputBeg = tempInputBeg;
			}

			return result;
		}

		++tempInputBeg;
		*tempOutputBeg++ = firstChar;
		*tempOutputBeg++ = secondChar;

		p_inputBeg = tempInputBeg;
		p_outputBeg = tempOutputBeg;
		return true;
	}
	else if (k_strictness == EStrictness::k_strict && firstChar >= k_lowBegin && firstChar <= k_lowEnd)
	{
		return false;
	}
	else
	{
		*p_outputBeg++ = firstChar;
		p_inputBeg = tempInputBeg;
		return true;
	}
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<char16_t, k_strictness>::copyCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg)
{
	// limits
	constexpr uint32_t k_highBegin = 0xD800;
	constexpr uint32_t k_highEnd = 0xDBFF;
	constexpr uint32_t k_lowBegin = 0xDC00;
	constexpr uint32_t k_lowEnd = 0xDFFF;

	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	const uint32_t firstChar = *tempInputBeg++;
	if (firstChar >= k_highBegin && firstChar <= k_highEnd)
	{
		// surrogate pairs
		const uint32_t secondChar = *tempInputBeg;
		if (secondChar < k_lowBegin || secondChar > k_lowEnd)
		{
			if (k_strictness == EStrictness::k_strict)
			{
				return false;
			}

			const bool result = Algorithms<char16_t, utf::k_relaxed>::fromUnicodeUnsafe(p_outputBeg, detail::k_unicodeCharStub);
			if (result)
			{
				p_inputBeg = tempInputBeg;
			}

			return result;
		}

		++tempInputBeg;
		*tempOutputBeg++ = firstChar;
		*tempOutputBeg++ = secondChar;

		p_inputBeg = tempInputBeg;
		p_outputBeg = tempOutputBeg;
		return true;
	}
	else if (k_strictness == EStrictness::k_strict && firstChar >= k_lowBegin && firstChar <= k_lowEnd)
	{
		return false;
	}
	else
	{
		*p_outputBeg++ = firstChar;
		p_inputBeg = tempInputBeg;
		return true;
	}
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char16_t, k_strictness>::nextSafe(IteratorT& p_beg, IteratorT p_end)
{
	if (p_beg >= p_end)
	{
		return false;
	}

	// limits
	constexpr uint32_t k_highBegin = 0xD800;
	constexpr uint32_t k_highEnd = 0xDBFF;
	constexpr uint32_t k_lowBegin = 0xDC00;
	constexpr uint32_t k_lowEnd = 0xDFFF;

	IteratorT tempUtf16Beg = p_beg;

	const uint32_t firstChar = *tempUtf16Beg++;
	if (firstChar >= k_highBegin && firstChar <= k_highEnd) 
	{
		// surrogate pairs
		if (tempUtf16Beg == p_end)
		{
			return false;
		}
		
		const uint32_t secondChar = *tempUtf16Beg;
		if (secondChar < k_lowBegin || secondChar > k_lowEnd)
		{
			if (k_strictness == EStrictness::k_strict)
			{
				return false;
			}
		}
		else
		{
			++tempUtf16Beg;
		}

		p_beg = tempUtf16Beg;
		return true;
	}
	else if (k_strictness == EStrictness::k_strict && firstChar >= k_lowBegin && firstChar <= k_lowEnd) 
	{
		return false;
	} 
	else 
	{
		p_beg = tempUtf16Beg;
		return true;
	}
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char16_t, k_strictness>::nextUnsafe(IteratorT& p_beg)
{
	// limits
	constexpr uint32_t k_highBegin = 0xD800;
	constexpr uint32_t k_highEnd = 0xDBFF;
	constexpr uint32_t k_lowBegin = 0xDC00;
	constexpr uint32_t k_lowEnd = 0xDFFF;

	IteratorT tempUtf16Beg = p_beg;

	const uint32_t firstChar = *tempUtf16Beg++;
	if (firstChar >= k_highBegin && firstChar <= k_highEnd) 
	{
		const uint32_t secondChar = *tempUtf16Beg;
		if (secondChar < k_lowBegin || secondChar > k_lowEnd)
		{
			if (k_strictness == EStrictness::k_strict)
			{
				return false;
			}
		}
		else
		{
			++tempUtf16Beg;
		}

		p_beg = tempUtf16Beg;
		return true;
	} 
	else if (k_strictness == EStrictness::k_strict && firstChar >= k_lowBegin && firstChar <= k_lowEnd) 
	{
		return false;
	} 
	else 
	{
		p_beg = tempUtf16Beg;
		return true;
	}
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
size_t utf::Algorithms<char16_t, k_strictness>::distance(IteratorT& p_beg, IteratorT p_end)
{
	size_t count = 0;
	while (nextSafe(p_beg, p_end))
	{
		++count;
	}

	return count;
}

/////////////////////////// char32_t ///////////////////////////

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char32_t, k_strictness>::fromUnicodeSafe(IteratorT& p_outputBeg, IteratorT p_outputEnd, uint32_t p_code)
{
	if (p_outputBeg >= p_outputEnd)
	{
		return false;
	}

	*p_outputBeg = static_cast<char32_t>(p_code);
	++p_outputBeg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char32_t, k_strictness>::fromUnicodeUnsafe(IteratorT& p_outputBeg, uint32_t p_code)
{
	*p_outputBeg = static_cast<char32_t>(p_code);
	++p_outputBeg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char32_t, k_strictness>::toUnicodeSafe(uint32_t& o_code, IteratorT& p_utf32Beg, IteratorT p_utf32End)
{
	if (p_utf32Beg >= p_utf32End)
	{
		return false;
	}

	o_code = *p_utf32Beg;
	++p_utf32Beg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char32_t, k_strictness>::toUnicodeUnsafe(uint32_t& o_code, IteratorT& p_utf32Beg)
{
	o_code = *p_utf32Beg;
	++p_utf32Beg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<char32_t, k_strictness>::copyCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	if (p_outputBeg >= p_outputEnd || p_inputBeg >= p_inputEnd)
	{
		return false;
	}

	*p_outputBeg = *p_inputBeg;
	++p_inputBeg;
	++p_outputBeg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<char32_t, k_strictness>::copyCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg)
{
	if (p_outputBeg >= p_outputEnd)
	{
		return false;
	}

	*p_outputBeg = *p_inputBeg;
	++p_inputBeg;
	++p_outputBeg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<char32_t, k_strictness>::copyCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	if (p_inputBeg >= p_inputEnd)
	{
		return false;
	}

	*p_outputBeg = *p_inputBeg;
	++p_inputBeg;
	++p_outputBeg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<char32_t, k_strictness>::copyCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg)
{
	*p_outputBeg = *p_inputBeg;
	++p_inputBeg;
	++p_outputBeg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char32_t, k_strictness>::nextSafe(IteratorT& p_beg, IteratorT p_end)
{
	if (p_beg < p_end)
	{
		++p_beg;
		return true;
	}
	return false;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<char32_t, k_strictness>::nextUnsafe(IteratorT& p_beg)
{
	++p_beg;
	return true;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
auto utf::Algorithms<char32_t, k_strictness>::distance(IteratorT& p_beg, IteratorT p_end) ->
	typename std::enable_if<
		std::is_convertible<decltype(p_end - p_beg), size_t>::value,
		size_t
	>::type
{
	const size_t count = p_end - p_beg;
	p_beg = p_end;
	return count;
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
auto utf::Algorithms<char32_t, k_strictness>::distance(IteratorT& p_beg, IteratorT p_end) ->
	typename std::enable_if<
		!std::is_convertible<decltype(p_end - p_beg), size_t>::value,
		size_t
	>::type
{
	size_t count = 0;
	while (p_beg != p_end)
	{
		++p_beg;
		++count;
	}
	return count;
}

/////////////////////////// wchar_t ///////////////////////////

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<wchar_t, k_strictness>::fromUnicodeSafe(IteratorT& p_outputBeg, IteratorT p_outputEnd, uint32_t p_code)
{
	return AliasAlgorithms::template fromUnicodeSafe<IteratorT>(p_outputBeg, p_outputEnd, p_code);
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<wchar_t, k_strictness>::fromUnicodeUnsafe(IteratorT& p_outputBeg, uint32_t p_code)
{
	return AliasAlgorithms::template fromUnicodeUnsafe<IteratorT>(p_outputBeg, p_code);
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<wchar_t, k_strictness>::toUnicodeSafe(uint32_t& o_code, IteratorT& p_wcharBeg, IteratorT p_wcharEnd)
{
	return AliasAlgorithms::template toUnicodeSafe<IteratorT>(o_code, p_wcharBeg, p_wcharEnd);
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<wchar_t, k_strictness>::toUnicodeUnsafe(uint32_t& o_code, IteratorT& p_wcharBeg)
{
	return AliasAlgorithms::template toUnicodeUnsafe<IteratorT>(o_code, p_wcharBeg);
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<wchar_t, k_strictness>::copyCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	return AliasAlgorithms::template copyCharSafe<OutputIteratorT, InputIteratorT>(p_outputBeg, p_outputEnd, p_inputBeg, p_inputEnd);
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<wchar_t, k_strictness>::copyCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg)
{
	return AliasAlgorithms::template copyCharSafe<OutputIteratorT, InputIteratorT>(p_outputBeg, p_outputEnd, p_inputBeg);
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<wchar_t, k_strictness>::copyCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	return AliasAlgorithms::template copyCharUnsafe<OutputIteratorT, InputIteratorT>(p_outputBeg, p_inputBeg, p_inputEnd);
}

template<utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Algorithms<wchar_t, k_strictness>::copyCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg)
{
	return AliasAlgorithms::template copyCharUnsafe<OutputIteratorT, InputIteratorT>(p_outputBeg, p_inputBeg);
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<wchar_t, k_strictness>::nextSafe(IteratorT& p_beg, IteratorT p_end)
{
	return AliasAlgorithms::template nextSafe<IteratorT>(p_beg, p_end);
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
bool utf::Algorithms<wchar_t, k_strictness>::nextUnsafe(IteratorT& p_beg)
{
	return AliasAlgorithms::template nextUnsafe<IteratorT>(p_beg);
}

template<utf::EStrictness k_strictness>
template<typename IteratorT>
size_t utf::Algorithms<wchar_t, k_strictness>::distance(IteratorT& p_beg, IteratorT p_end)
{
	return AliasAlgorithms::template distance<IteratorT>(p_beg, p_end);
}