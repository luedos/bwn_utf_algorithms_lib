#pragma once

#include "converter.hpp"

#include <iterator>

template<typename OutputCharT, typename InputCharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<OutputCharT, InputCharT, k_strictness>::convertCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	uint32_t code;
	if (InputAlgorithms::template toUnicodeSafe<InputIteratorT>(code, tempInputBeg, p_inputEnd)
		&& OutputAlgorithms::template fromUnicodeSafe<OutputIteratorT>(tempOutputBeg, p_outputEnd, code))
	{
		p_inputBeg = tempInputBeg;
		p_outputBeg = tempOutputBeg;
		return true;
	}

	return false;
}


template<typename OutputCharT, typename InputCharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<OutputCharT, InputCharT, k_strictness>::convertCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg)
{
	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	uint32_t code;
	if (InputAlgorithms::template toUnicodeUnsafe<InputIteratorT>(code, tempInputBeg)
		&& OutputAlgorithms::template fromUnicodeSafe<OutputIteratorT>(tempOutputBeg, p_outputEnd, code))
	{
		p_inputBeg = tempInputBeg;
		p_outputBeg = tempOutputBeg;
		return true;
	}

	return false;
}

template<typename OutputCharT, typename InputCharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<OutputCharT, InputCharT, k_strictness>::convertCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	uint32_t code;
	if (InputAlgorithms::template toUnicodeSafe<InputIteratorT>(code, tempInputBeg, p_inputEnd)
		&& OutputAlgorithms::template fromUnicodeUnsafe<OutputIteratorT>(tempOutputBeg, code))
	{
		p_inputBeg = tempInputBeg;
		p_outputBeg = tempOutputBeg;
		return true;
	}

	return false;
}

template<typename OutputCharT, typename InputCharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<OutputCharT, InputCharT, k_strictness>::convertCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg)
{
	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	uint32_t code;
	if (InputAlgorithms::template toUnicodeUnsafe<InputIteratorT>(code, tempInputBeg)
		&& OutputAlgorithms::template fromUnicodeUnsafe<OutputIteratorT>(tempOutputBeg, code))
	{
		p_inputBeg = tempInputBeg;
		p_outputBeg = tempOutputBeg;
		return true;
	}

	return false;
}

template<typename OutputCharT, typename InputCharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<OutputCharT, InputCharT, k_strictness>::covertStringSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	while (tempOutputBeg != p_outputEnd && tempInputBeg != p_inputEnd)
	{
		uint32_t code;
		if (!InputAlgorithms::template toUnicodeSafe<InputIteratorT>(code, tempInputBeg, p_inputEnd)
			|| !OutputAlgorithms::template fromUnicodeSafe<OutputIteratorT>(tempOutputBeg, p_outputEnd, code))
		{
			return false;
		}
	}

	p_inputBeg = tempInputBeg;
	p_outputBeg = tempOutputBeg;
	return true;
}

template<typename OutputCharT, typename InputCharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<OutputCharT, InputCharT, k_strictness>::covertStringUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	while (tempInputBeg != p_inputEnd)
	{
		uint32_t code;
		if (!InputAlgorithms::template toUnicodeSafe<InputIteratorT>(code, tempInputBeg, p_inputEnd)
			|| !OutputAlgorithms::template fromUnicodeUnsafe<OutputIteratorT>(tempOutputBeg, code))
		{
			return false;
		}
	}

	p_inputBeg = tempInputBeg;
	p_outputBeg = tempOutputBeg;
	return true;
}

template<typename OutputCharT, typename InputCharT, utf::EStrictness k_strictness>
template<typename ContainerT, typename InputIteratorT>
ContainerT utf::Converter<OutputCharT, InputCharT, k_strictness>::convertToContainer(
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	ContainerT ret;
	
	struct LocalBackInserter
	{
	public:
		LocalBackInserter(ContainerT& p_container)
			: m_container(&p_container)
		{}

		LocalBackInserter(const LocalBackInserter& p_other)
			: m_container(p_other.m_container)
		{}
		LocalBackInserter& operator=(const LocalBackInserter& p_other)
		{
			m_container = p_other.m_container;
			return *this;
		}

		LocalBackInserter& operator=(OutputCharT p_char)
		{
			m_container->push_back(p_char);
			return *this;
		}
		LocalBackInserter& operator*() { return *this; }
		LocalBackInserter& operator++(int) { return *this; }
		LocalBackInserter& operator++() { return *this; }

	private:
		ContainerT* m_container;
	};
	LocalBackInserter tempOutputIt(ret);

	InputIteratorT tempInputBeg = p_inputBeg;

	while (tempInputBeg != p_inputEnd)
	{
		uint32_t code;
		if (!InputAlgorithms::template toUnicodeSafe<InputIteratorT>(code, tempInputBeg, p_inputEnd)
			|| !OutputAlgorithms::template fromUnicodeUnsafe<LocalBackInserter>(tempOutputIt, code))
		{
			// To please nrvo.
			ret.clear();
			return ret;
		}
	}

	p_inputBeg = tempInputBeg;
	return ret;
}

template<typename OutputCharT, typename InputCharT, utf::EStrictness k_strictness>
template<typename OutputContainerT, typename InputContainerT>
OutputContainerT utf::Converter<OutputCharT, InputCharT, k_strictness>::convertToContainer(const InputContainerT& p_container)
{
	auto beginIt = std::begin(p_container);
	using Iterator = typename std::remove_reference<decltype(beginIt)>::type;
	return convertToContainer<OutputContainerT, Iterator>(beginIt, p_container.end());
}

template<typename CharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<CharT, CharT, k_strictness>::convertCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	return CharAlgorithms::template copyCharSafe<OutputIteratorT, InputIteratorT>(p_outputBeg, p_outputEnd, p_inputBeg, p_inputEnd);
}


template<typename CharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<CharT, CharT, k_strictness>::convertCharSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg)
{
	return CharAlgorithms::template copyCharSafe<OutputIteratorT, InputIteratorT>(p_outputBeg, p_outputEnd, p_inputBeg);
}

template<typename CharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<CharT, CharT, k_strictness>::convertCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	return CharAlgorithms::template copyCharUnsafe<OutputIteratorT, InputIteratorT>(p_outputBeg, p_inputBeg, p_inputEnd);
}

template<typename CharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<CharT, CharT, k_strictness>::convertCharUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg)
{
	return CharAlgorithms::template copyCharUnsafe<OutputIteratorT, InputIteratorT>(p_outputBeg, p_inputBeg);
}

template<typename CharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<CharT, CharT, k_strictness>::covertStringSafe(
	OutputIteratorT& p_outputBeg, 
	OutputIteratorT p_outputEnd, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	while (tempOutputBeg != p_outputEnd && tempInputBeg != p_inputEnd)
	{
		if (!CharAlgorithms::template copyCharSafe<OutputIteratorT, InputIteratorT>(
			tempOutputBeg, 
			p_outputEnd, 
			tempInputBeg, 
			p_inputEnd))
		{
			return false;
		}
	}

	p_inputBeg = tempInputBeg;
	p_outputBeg = tempOutputBeg;
	return true;
}

template<typename CharT, utf::EStrictness k_strictness>
template<typename OutputIteratorT, typename InputIteratorT>
bool utf::Converter<CharT, CharT, k_strictness>::covertStringUnsafe(
	OutputIteratorT& p_outputBeg, 
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	InputIteratorT tempInputBeg = p_inputBeg;
	OutputIteratorT tempOutputBeg = p_outputBeg;

	while (tempInputBeg != p_inputEnd)
	{
		if (!CharAlgorithms::template copyCharUnsafe<OutputIteratorT, InputIteratorT>(tempOutputBeg, tempInputBeg, p_inputEnd))
		{
			return false;
		}
	}

	p_inputBeg = tempInputBeg;
	p_outputBeg = tempOutputBeg;
	return true;
}

template<typename CharT, utf::EStrictness k_strictness>
template<typename ContainerT, typename InputIteratorT>
ContainerT utf::Converter<CharT, CharT, k_strictness>::convertToContainer(
	InputIteratorT& p_inputBeg, 
	InputIteratorT p_inputEnd)
{
	ContainerT ret;
	struct LocalBackInserter
	{
	public:
		explicit LocalBackInserter(ContainerT& p_container)
			: m_container(&p_container)
		{}

		LocalBackInserter(const LocalBackInserter& p_other)
			: m_container(p_other.m_container)
		{}
		
		LocalBackInserter& operator=(const LocalBackInserter& p_other)
		{
			m_container = p_other.m_container;
			return *this;
		}

		LocalBackInserter& operator=(CharT p_char)
		{
			m_container->push_back(p_char);
			return *this;
		}
		LocalBackInserter& operator*() { return *this; }
		LocalBackInserter& operator++(int) { return *this; }
		LocalBackInserter& operator++() { return *this; }

	private:
		ContainerT* m_container;
	};
	LocalBackInserter tempOutputIt(ret);
	
	InputIteratorT tempInputBeg = p_inputBeg;

	while (tempInputBeg != p_inputEnd)
	{
		if (!CharAlgorithms::template copyCharUnsafe<LocalBackInserter, InputIteratorT>(tempOutputIt, tempInputBeg, p_inputEnd))
		{
			// To please nrvo.
			ret.clear();
			return ret;
		}
	}

	p_inputBeg = tempInputBeg;
	return ret;
}

template<typename CharT, utf::EStrictness k_strictness>
template<typename OutputContainerT, typename InputContainerT>
OutputContainerT utf::Converter<CharT, CharT, k_strictness>::convertToContainer(const InputContainerT& p_container)
{
	auto beginIt = std::begin(p_container);
	using Iterator = typename std::remove_reference<decltype(beginIt)>::type;
	return convertToContainer<OutputContainerT, Iterator>(beginIt, p_container.end());
}

#if ((defined(_MSVC_LANG) && _MSVC_LANG >= 201402L) || __cplusplus >= 201402L)
    #define UTF_ALGORITHMS_CONSTEXPR constexpr
#else
	#define UTF_ALGORITHMS_CONSTEXPR
#endif

template<utf::EStrictness k_strictness>
UTF_ALGORITHMS_CONSTEXPR bool utf::IterativeConverter<utf::Char8, k_strictness>::push(const Char8 _char, char32_t& o_output)
{
	using Algorithms = Algorithms<Char8, k_strictness>;

	if (Algorithms::isAsciiByte(_char))
	{
		m_trailingBytesLeft = 0;
		o_output = _char;
		return true;
	}

	const uint8_t byte = static_cast<uint8_t>(_char);
	if (Algorithms::isHeaderByte(byte))
	{
		const bool hadExtraBytes = m_trailingBytesLeft != 0;

		m_trailingBytes = Algorithms::getExtraBytesFromHeader(byte);
		m_trailingBytesLeft = m_trailingBytes;
		m_returnCode = byte;

		if (k_strictness == EStrictness::k_relaxed)
		{
			if (hadExtraBytes)
			{
				o_output = detail::k_unicodeCharStub;
				return true;
			}
		}

		return false;
	}

	if (m_trailingBytesLeft == 0)
	{
		if (k_strictness == EStrictness::k_relaxed)
		{
			o_output = detail::k_unicodeCharStub;
			return true;
		}

		return false;
	}

	m_returnCode <<= 6;
	m_returnCode += byte;
	--m_trailingBytesLeft;

	if (m_trailingBytesLeft == 0)
	{
		constexpr uint32_t k_utf8Offsets[6] = { 0x00000000UL, 0x00003080UL, 0x000E2080UL, 0x03C82080UL, 0xFA082080UL, 0x82082080UL };
		m_returnCode -= k_utf8Offsets[m_trailingBytes];
		o_output = m_returnCode;
		return true;
	}

	return false;
}

template<utf::EStrictness k_strictness>
UTF_ALGORITHMS_CONSTEXPR void utf::IterativeConverter<utf::Char8, k_strictness>::clear()
{
	m_returnCode = 0;
	m_trailingBytes = 0;
	m_trailingBytesLeft = 0;
}

template<utf::EStrictness k_strictness>
UTF_ALGORITHMS_CONSTEXPR bool utf::IterativeConverter<char16_t, k_strictness>::push(const char16_t _char, char32_t& o_output)
{
	// limits
	constexpr uint32_t k_highBegin = 0xD800;
	constexpr uint32_t k_highEnd = 0xDBFF;
	constexpr uint32_t k_lowBegin = 0xDC00;
	constexpr uint32_t k_lowEnd = 0xDFFF;
	constexpr int k_shift = 10;
	constexpr uint32_t k_base = 0x0010000UL;

	if (_char >= k_highBegin && _char <= k_highEnd)
	{
		const bool hadStart = m_first != 0;

		m_first = _char;
		if (k_strictness == EStrictness::k_relaxed && hadStart)
		{
			o_output = detail::k_unicodeCharStub;
			return true;
		}

		return false;
	}
	else if (_char >= k_lowBegin && _char <= k_lowEnd)
	{
		if (m_first == 0)
		{
			if (k_strictness == EStrictness::k_relaxed)
			{
				o_output = detail::k_unicodeCharStub;
				return true;
			}

			return false;
		}

		o_output = ((m_first - k_highBegin) << k_shift) + (_char - k_lowBegin) + k_base;
		m_first = 0;
		return true;
	}

	m_first = 0;
	o_output = _char;
	return true;
}

template<utf::EStrictness k_strictness>
UTF_ALGORITHMS_CONSTEXPR void utf::IterativeConverter<char16_t, k_strictness>::clear()
{
	m_first = 0;
}

#undef UTF_ALGORITHMS_CONSTEXPR