#pragma once

#include "algorithms.hpp"

/////////////////////////// char8 ///////////////////////////
extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::fromUnicodeSafe<utf::Char8*>(utf::Char8*&, utf::Char8*, uint32_t);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::fromUnicodeSafe<utf::Char8*>(utf::Char8*&, utf::Char8*, uint32_t);

extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::fromUnicodeUnsafe<utf::Char8*>(utf::Char8*&, uint32_t);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::fromUnicodeUnsafe<utf::Char8*>(utf::Char8*&, uint32_t);

extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::toUnicodeSafe<const utf::Char8*>(uint32_t&, const utf::Char8*&, const utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::toUnicodeSafe<const utf::Char8*>(uint32_t&, const utf::Char8*&, const utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::toUnicodeSafe<utf::Char8*>(uint32_t&, utf::Char8*&, utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::toUnicodeSafe<utf::Char8*>(uint32_t&, utf::Char8*&, utf::Char8*);

extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::toUnicodeUnsafe<const utf::Char8*>(uint32_t&, const utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::toUnicodeUnsafe<const utf::Char8*>(uint32_t&, const utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::toUnicodeUnsafe<utf::Char8*>(uint32_t&, utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::toUnicodeUnsafe<utf::Char8*>(uint32_t&, utf::Char8*&);

extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::copyCharSafe<utf::Char8*, const utf::Char8*>(utf::Char8*&, utf::Char8*, const utf::Char8*&, const utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::copyCharSafe<utf::Char8*, utf::Char8*>(utf::Char8*&, utf::Char8*, utf::Char8*&, utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::copyCharSafe<utf::Char8*, const utf::Char8*>(utf::Char8*&, utf::Char8*, const utf::Char8*&, const utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::copyCharSafe<utf::Char8*, utf::Char8*>(utf::Char8*&, utf::Char8*, utf::Char8*&, utf::Char8*);

extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::copyCharSafe<utf::Char8*, const utf::Char8*>(utf::Char8*&, utf::Char8*, const utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::copyCharSafe<utf::Char8*, utf::Char8*>(utf::Char8*&, utf::Char8*, utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::copyCharSafe<utf::Char8*, const utf::Char8*>(utf::Char8*&, utf::Char8*, const utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::copyCharSafe<utf::Char8*, utf::Char8*>(utf::Char8*&, utf::Char8*, utf::Char8*&);

extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::copyCharUnsafe<utf::Char8*, const utf::Char8*>(utf::Char8*&, const utf::Char8*&, const utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::copyCharUnsafe<utf::Char8*, utf::Char8*>(utf::Char8*&, utf::Char8*&, utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::copyCharUnsafe<utf::Char8*, const utf::Char8*>(utf::Char8*&, const utf::Char8*&, const utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::copyCharUnsafe<utf::Char8*, utf::Char8*>(utf::Char8*&, utf::Char8*&, utf::Char8*);

extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::copyCharUnsafe<utf::Char8*, const utf::Char8*>(utf::Char8*&, const utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::copyCharUnsafe<utf::Char8*, utf::Char8*>(utf::Char8*&, utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::copyCharUnsafe<utf::Char8*, const utf::Char8*>(utf::Char8*&, const utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::copyCharUnsafe<utf::Char8*, utf::Char8*>(utf::Char8*&, utf::Char8*&);

extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::nextSafe<const utf::Char8*>(const utf::Char8*&, const utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::nextSafe<utf::Char8*>(utf::Char8*&, utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::nextSafe<const utf::Char8*>(const utf::Char8*&, const utf::Char8*);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::nextSafe<utf::Char8*>(utf::Char8*&, utf::Char8*);

extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::nextUnsafe<const utf::Char8*>(const utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_strict>::nextUnsafe<utf::Char8*>(utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::nextUnsafe<const utf::Char8*>(const utf::Char8*&);
extern template bool utf::Algorithms<utf::Char8, utf::k_relaxed>::nextUnsafe<utf::Char8*>(utf::Char8*&);

extern template size_t utf::Algorithms<utf::Char8, utf::k_strict>::distance<const utf::Char8*>(const utf::Char8*&, const utf::Char8*);
extern template size_t utf::Algorithms<utf::Char8, utf::k_strict>::distance<utf::Char8*>(utf::Char8*&, utf::Char8*);
extern template size_t utf::Algorithms<utf::Char8, utf::k_relaxed>::distance<const utf::Char8*>(const utf::Char8*&, const utf::Char8*);
extern template size_t utf::Algorithms<utf::Char8, utf::k_relaxed>::distance<utf::Char8*>(utf::Char8*&, utf::Char8*);

/////////////////////////// char16_t ///////////////////////////
extern template bool utf::Algorithms<char16_t, utf::k_strict>::fromUnicodeSafe<char16_t*>(char16_t*&, char16_t*, uint32_t);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::fromUnicodeSafe<char16_t*>(char16_t*&, char16_t*, uint32_t);

extern template bool utf::Algorithms<char16_t, utf::k_strict>::fromUnicodeUnsafe<char16_t*>(char16_t*&, uint32_t);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::fromUnicodeUnsafe<char16_t*>(char16_t*&, uint32_t);

extern template bool utf::Algorithms<char16_t, utf::k_strict>::toUnicodeSafe<const char16_t*>(uint32_t&, const char16_t*&, const char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::toUnicodeSafe<const char16_t*>(uint32_t&, const char16_t*&, const char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_strict>::toUnicodeSafe<char16_t*>(uint32_t&, char16_t*&, char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::toUnicodeSafe<char16_t*>(uint32_t&, char16_t*&, char16_t*);

extern template bool utf::Algorithms<char16_t, utf::k_strict>::toUnicodeUnsafe<const char16_t*>(uint32_t&, const char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::toUnicodeUnsafe<const char16_t*>(uint32_t&, const char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_strict>::toUnicodeUnsafe<char16_t*>(uint32_t&, char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::toUnicodeUnsafe<char16_t*>(uint32_t&, char16_t*&);

extern template bool utf::Algorithms<char16_t, utf::k_strict>::copyCharSafe<char16_t*, const char16_t*>(char16_t*&, char16_t*, const char16_t*&, const char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_strict>::copyCharSafe<char16_t*, char16_t*>(char16_t*&, char16_t*, char16_t*&, char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::copyCharSafe<char16_t*, const char16_t*>(char16_t*&, char16_t*, const char16_t*&, const char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::copyCharSafe<char16_t*, char16_t*>(char16_t*&, char16_t*, char16_t*&, char16_t*);

extern template bool utf::Algorithms<char16_t, utf::k_strict>::copyCharSafe<char16_t*, const char16_t*>(char16_t*&, char16_t*, const char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_strict>::copyCharSafe<char16_t*, char16_t*>(char16_t*&, char16_t*, char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::copyCharSafe<char16_t*, const char16_t*>(char16_t*&, char16_t*, const char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::copyCharSafe<char16_t*, char16_t*>(char16_t*&, char16_t*, char16_t*&);

extern template bool utf::Algorithms<char16_t, utf::k_strict>::copyCharUnsafe<char16_t*, const char16_t*>(char16_t*&, const char16_t*&, const char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_strict>::copyCharUnsafe<char16_t*, char16_t*>(char16_t*&, char16_t*&, char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::copyCharUnsafe<char16_t*, const char16_t*>(char16_t*&, const char16_t*&, const char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::copyCharUnsafe<char16_t*, char16_t*>(char16_t*&, char16_t*&, char16_t*);

extern template bool utf::Algorithms<char16_t, utf::k_strict>::copyCharUnsafe<char16_t*, const char16_t*>(char16_t*&, const char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_strict>::copyCharUnsafe<char16_t*, char16_t*>(char16_t*&, char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::copyCharUnsafe<char16_t*, const char16_t*>(char16_t*&, const char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::copyCharUnsafe<char16_t*, char16_t*>(char16_t*&, char16_t*&);

extern template bool utf::Algorithms<char16_t, utf::k_strict>::nextSafe<const char16_t*>(const char16_t*&, const char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_strict>::nextSafe<char16_t*>(char16_t*&, char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::nextSafe<const char16_t*>(const char16_t*&, const char16_t*);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::nextSafe<char16_t*>(char16_t*&, char16_t*);

extern template bool utf::Algorithms<char16_t, utf::k_strict>::nextUnsafe<const char16_t*>(const char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_strict>::nextUnsafe<char16_t*>(char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::nextUnsafe<const char16_t*>(const char16_t*&);
extern template bool utf::Algorithms<char16_t, utf::k_relaxed>::nextUnsafe<char16_t*>(char16_t*&);

extern template size_t utf::Algorithms<char16_t, utf::k_strict>::distance<const char16_t*>(const char16_t*&, const char16_t*);
extern template size_t utf::Algorithms<char16_t, utf::k_strict>::distance<char16_t*>(char16_t*&, char16_t*);
extern template size_t utf::Algorithms<char16_t, utf::k_relaxed>::distance<const char16_t*>(const char16_t*&, const char16_t*);
extern template size_t utf::Algorithms<char16_t, utf::k_relaxed>::distance<char16_t*>(char16_t*&, char16_t*);

/////////////////////////// char32_t ///////////////////////////
extern template bool utf::Algorithms<char32_t, utf::k_strict>::fromUnicodeSafe<char32_t*>(char32_t*&, char32_t*, uint32_t);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::fromUnicodeSafe<char32_t*>(char32_t*&, char32_t*, uint32_t);

extern template bool utf::Algorithms<char32_t, utf::k_strict>::fromUnicodeUnsafe<char32_t*>(char32_t*&, uint32_t);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::fromUnicodeUnsafe<char32_t*>(char32_t*&, uint32_t);

extern template bool utf::Algorithms<char32_t, utf::k_strict>::toUnicodeSafe<const char32_t*>(uint32_t&, const char32_t*&, const char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::toUnicodeSafe<const char32_t*>(uint32_t&, const char32_t*&, const char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_strict>::toUnicodeSafe<char32_t*>(uint32_t&, char32_t*&, char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::toUnicodeSafe<char32_t*>(uint32_t&, char32_t*&, char32_t*);

extern template bool utf::Algorithms<char32_t, utf::k_strict>::toUnicodeUnsafe<const char32_t*>(uint32_t&, const char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::toUnicodeUnsafe<const char32_t*>(uint32_t&, const char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_strict>::toUnicodeUnsafe<char32_t*>(uint32_t&, char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::toUnicodeUnsafe<char32_t*>(uint32_t&, char32_t*&);

extern template bool utf::Algorithms<char32_t, utf::k_strict>::copyCharSafe<char32_t*, const char32_t*>(char32_t*&, char32_t*, const char32_t*&, const char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_strict>::copyCharSafe<char32_t*, char32_t*>(char32_t*&, char32_t*, char32_t*&, char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::copyCharSafe<char32_t*, const char32_t*>(char32_t*&, char32_t*, const char32_t*&, const char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::copyCharSafe<char32_t*, char32_t*>(char32_t*&, char32_t*, char32_t*&, char32_t*);

extern template bool utf::Algorithms<char32_t, utf::k_strict>::copyCharSafe<char32_t*, const char32_t*>(char32_t*&, char32_t*, const char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_strict>::copyCharSafe<char32_t*, char32_t*>(char32_t*&, char32_t*, char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::copyCharSafe<char32_t*, const char32_t*>(char32_t*&, char32_t*, const char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::copyCharSafe<char32_t*, char32_t*>(char32_t*&, char32_t*, char32_t*&);

extern template bool utf::Algorithms<char32_t, utf::k_strict>::copyCharUnsafe<char32_t*, const char32_t*>(char32_t*&, const char32_t*&, const char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_strict>::copyCharUnsafe<char32_t*, char32_t*>(char32_t*&, char32_t*&, char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::copyCharUnsafe<char32_t*, const char32_t*>(char32_t*&, const char32_t*&, const char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::copyCharUnsafe<char32_t*, char32_t*>(char32_t*&, char32_t*&, char32_t*);

extern template bool utf::Algorithms<char32_t, utf::k_strict>::copyCharUnsafe<char32_t*, const char32_t*>(char32_t*&, const char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_strict>::copyCharUnsafe<char32_t*, char32_t*>(char32_t*&, char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::copyCharUnsafe<char32_t*, const char32_t*>(char32_t*&, const char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::copyCharUnsafe<char32_t*, char32_t*>(char32_t*&, char32_t*&);

extern template bool utf::Algorithms<char32_t, utf::k_strict>::nextSafe<const char32_t*>(const char32_t*&, const char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_strict>::nextSafe<char32_t*>(char32_t*&, char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::nextSafe<const char32_t*>(const char32_t*&, const char32_t*);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::nextSafe<char32_t*>(char32_t*&, char32_t*);

extern template bool utf::Algorithms<char32_t, utf::k_strict>::nextUnsafe<const char32_t*>(const char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_strict>::nextUnsafe<char32_t*>(char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::nextUnsafe<const char32_t*>(const char32_t*&);
extern template bool utf::Algorithms<char32_t, utf::k_relaxed>::nextUnsafe<char32_t*>(char32_t*&);
