#define CATCH_CONFIG_MAIN
#include "catch.hpp"

#include "utf-algorithms/algorithms.hpp"
#include "utf-algorithms/converter.hpp"

#include <string>

namespace test
{

#define BWN_STRING_LITERAL(_charT, _string) \
[]() -> const _charT* { \
	static_assert( \
		std::is_same<_charT, char>::value \
		|| std::is_same<_charT, wchar_t>::value \
		|| std::is_same<_charT, char16_t>::value \
		|| std::is_same<_charT, char32_t>::value, \
		"Unknown char type."); \
	if (std::is_same<_charT, char>::value) return (const _charT*) u8 ## _string; \
	else if (std::is_same<_charT, wchar_t>::value) return (const _charT*) L ## _string; \
	else if (std::is_same<_charT, char16_t>::value) return (const _charT*) u ## _string; \
	else if (std::is_same<_charT, char32_t>::value) return (const _charT*) U ## _string; \
}()

template<typename CharT>
CharT* findStrTermination(CharT* p_str)
{
	while (*p_str)
	{
		++p_str;
	}

	return p_str;
}

template<typename CharT>
bool compareBytes(
	const CharT* p_firstBeg, 
	const CharT*const p_firstEnd,
	const CharT* p_secondBeg,
	const CharT*const p_secondEnd )
{
	for (; p_firstBeg < p_firstEnd && p_secondBeg < p_secondEnd; ++p_firstBeg, ++p_secondBeg)
	{
		if (*p_firstBeg != *p_secondBeg)
		{
			return false;
		}
	}

	return p_firstBeg == p_firstEnd && p_secondBeg == p_secondEnd;
}

template<typename CharT>
void strncpy(CharT* p_destination, const CharT* p_source, size_t p_count)
{
	while (p_count > 0)
	{
		*p_destination = *p_source;
		++p_destination;
		++p_source;
		--p_count;
	}
}

template<typename FirstCharT, typename SecondCharT>
bool compareUtf(
	const FirstCharT* p_firstBeg, 
	const FirstCharT*const p_firstEnd,
	const SecondCharT* p_secondBeg,
	const SecondCharT*const p_secondEnd )
{
	for (; p_firstBeg < p_firstEnd && p_secondBeg < p_secondEnd;)
	{
		uint32_t firstCode;
		uint32_t secondCode;
		if (!utf::Algorithms<FirstCharT>::toUnicodeSafe(firstCode, p_firstBeg, p_firstEnd) 
			|| !utf::Algorithms<SecondCharT>::toUnicodeSafe(secondCode, p_secondBeg, p_secondEnd))
		{
			return false;
		}

		if (firstCode != secondCode)
		{
			return false;
		}
	}

	return p_firstBeg == p_firstEnd && p_secondBeg == p_secondEnd;
}

} // namespace test

////////////////////////////////////////////////////////////////
//////////////////////// Success checks ////////////////////////
////////////////////////////////////////////////////////////////

TEST_CASE("Valid Conversion to Unicode")
{

#define BWN_SECTION(p_name, p_charT, ...) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		const Char*const tests[] \
		{ \
			BWN_STRING_LITERAL(p_charT, "\U00000061"), \
			BWN_STRING_LITERAL(p_charT, "\U00000430"), \
			BWN_STRING_LITERAL(p_charT, "\U00002661"), \
			BWN_STRING_LITERAL(p_charT, "\U0001F5A4") \
		}; \
		const uint32_t answers[] \
		{ \
			0x00000061, \
			0x00000430, \
			0x00002661, \
			0x0001F5A4 \
		}; \
		const Char*const* testIt = std::begin(tests); \
		const Char*const* testEnd = std::end(tests); \
		const uint32_t* answerIt = std::begin(answers); \
		const uint32_t* answerEnd = std::end(answers); \
		for (; testIt != testEnd && answerIt != answerEnd; ++testIt, ++answerIt) \
		{ \
			const Char* strIt = *testIt; \
			const Char*const strEnd = test::findStrTermination(strIt); \
			uint32_t outputCode; \
			REQUIRE(__VA_ARGS__); \
			REQUIRE(outputCode == *answerIt); \
			REQUIRE(strIt == strEnd); \
		} \
	}

	BWN_SECTION("Utf8 safe/strict", utf::Char8, utf::Algorithms<Char, utf::k_strict>::toUnicodeSafe(outputCode, strIt, strEnd))
	BWN_SECTION("Utf8 safe/not strict", utf::Char8, utf::Algorithms<Char, utf::k_relaxed>::toUnicodeSafe(outputCode, strIt, strEnd))
	BWN_SECTION("Utf8 unsafe/strict", utf::Char8, utf::Algorithms<Char, utf::k_strict>::toUnicodeUnsafe(outputCode, strIt))
	BWN_SECTION("Utf8 unsafe/not strict", utf::Char8, utf::Algorithms<Char, utf::k_relaxed>::toUnicodeUnsafe(outputCode, strIt))

	BWN_SECTION("Utf16 safe/strict", char16_t, utf::Algorithms<Char, utf::k_strict>::toUnicodeSafe(outputCode, strIt, strEnd))
	BWN_SECTION("Utf16 safe/not strict", char16_t, utf::Algorithms<Char, utf::k_relaxed>::toUnicodeSafe(outputCode, strIt, strEnd))
	BWN_SECTION("Utf16 unsafe/strict", char16_t, utf::Algorithms<Char, utf::k_strict>::toUnicodeUnsafe(outputCode, strIt))
	BWN_SECTION("Utf16 unsafe/not strict", char16_t, utf::Algorithms<Char, utf::k_relaxed>::toUnicodeUnsafe(outputCode, strIt))

	BWN_SECTION("Utf32 safe/strict", char32_t, utf::Algorithms<Char, utf::k_strict>::toUnicodeSafe(outputCode, strIt, strEnd))
	BWN_SECTION("Utf32 safe/not strict", char32_t, utf::Algorithms<Char, utf::k_relaxed>::toUnicodeSafe(outputCode, strIt, strEnd))
	BWN_SECTION("Utf32 unsafe/strict", char32_t, utf::Algorithms<Char, utf::k_strict>::toUnicodeUnsafe(outputCode, strIt))
	BWN_SECTION("Utf32 unsafe/not strict", char32_t, utf::Algorithms<Char, utf::k_relaxed>::toUnicodeUnsafe(outputCode, strIt))

	BWN_SECTION("Wchar safe/strict", wchar_t, utf::Algorithms<Char, utf::k_strict>::toUnicodeSafe(outputCode, strIt, strEnd))
	BWN_SECTION("Wchar safe/not strict", wchar_t, utf::Algorithms<Char, utf::k_relaxed>::toUnicodeSafe(outputCode, strIt, strEnd))
	BWN_SECTION("Wchar unsafe/strict", wchar_t, utf::Algorithms<Char, utf::k_strict>::toUnicodeUnsafe(outputCode, strIt))
	BWN_SECTION("Wchar unsafe/not strict", wchar_t, utf::Algorithms<Char, utf::k_relaxed>::toUnicodeUnsafe(outputCode, strIt))

#undef BWN_SECTION
}

TEST_CASE("Invalid Conversion to Unicode")
{
#define BWN_INVALID_ITERATOR_SECTION(p_name, p_charT, ...) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		const Char localChar = 'a'; \
		{ \
			uint32_t outputCode = 0; \
			const Char* startIt = &localChar; \
			const Char*const invalidEndIt = startIt; \
			REQUIRE(__VA_ARGS__); \
			REQUIRE(startIt == &localChar); \
			REQUIRE(outputCode == 0); \
		} \
		{ \
			uint32_t outputCode = 0; \
			const Char* startIt = &localChar; \
			const Char*const invalidEndIt = startIt - 4; \
			REQUIRE(__VA_ARGS__); \
			REQUIRE(startIt == &localChar); \
			REQUIRE(outputCode == 0); \
		} \
	}

	BWN_INVALID_ITERATOR_SECTION("Invalid iterators (Utf8 strict)", utf::Char8, !utf::Algorithms<Char, utf::k_strict>::toUnicodeSafe(outputCode, startIt, invalidEndIt))
	BWN_INVALID_ITERATOR_SECTION("Invalid iterators (Utf16 strict)", char16_t, !utf::Algorithms<Char, utf::k_strict>::toUnicodeSafe(outputCode, startIt, invalidEndIt))
	BWN_INVALID_ITERATOR_SECTION("Invalid iterators (Utf32 strict)", char32_t, !utf::Algorithms<Char, utf::k_strict>::toUnicodeSafe(outputCode, startIt, invalidEndIt))
	BWN_INVALID_ITERATOR_SECTION("Invalid iterators (Wchar strict)", wchar_t, !utf::Algorithms<Char, utf::k_strict>::toUnicodeSafe(outputCode, startIt, invalidEndIt))

	BWN_INVALID_ITERATOR_SECTION("Invalid iterators (Utf8 not strict)", utf::Char8, !utf::Algorithms<Char, utf::k_relaxed>::toUnicodeSafe(outputCode, startIt, invalidEndIt))
	BWN_INVALID_ITERATOR_SECTION("Invalid iterators (Utf16 not strict)", char16_t, !utf::Algorithms<Char, utf::k_relaxed>::toUnicodeSafe(outputCode, startIt, invalidEndIt))
	BWN_INVALID_ITERATOR_SECTION("Invalid iterators (Utf32 not strict)", char32_t, !utf::Algorithms<Char, utf::k_relaxed>::toUnicodeSafe(outputCode, startIt, invalidEndIt))
	BWN_INVALID_ITERATOR_SECTION("Invalid iterators (Wchar not strict)", wchar_t, !utf::Algorithms<Char, utf::k_relaxed>::toUnicodeSafe(outputCode, startIt, invalidEndIt))

#undef BWN_INVALID_ITERATOR_SECTION

#define BWN_SECTION_BEGIN(p_name, p_charT, p_invalidString, p_invalidCharCount) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		const Char*const invalidStr = p_invalidString; \
		const Char*const invalidStrEnd = invalidStr + p_invalidCharCount; \
		const Char* startIt = invalidStr; \
		const Char*const endIt = test::findStrTermination(invalidStr); \
		uint32_t outputCode = 0; \
		(void)invalidStrEnd; \
		(void)endIt; \

#define BWN_SECTION_END() }

	BWN_SECTION_BEGIN("Utf8 strict/safe", utf::Char8, "\xF0\x9F\x96\xF0\x9F\x96\xA4", 3)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::toUnicodeSafe(outputCode, startIt, endIt));
		REQUIRE(startIt == invalidStr);
		REQUIRE(outputCode == 0);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf8 relaxed/safe", utf::Char8, "\xF0\x9F\x96\xF0\x9F\x96\xA4", 3)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::toUnicodeSafe(outputCode, startIt, endIt));
		REQUIRE(startIt == invalidStrEnd);
		REQUIRE(outputCode == utf::detail::k_unicodeCharStub);
	BWN_SECTION_END()
	
	BWN_SECTION_BEGIN("Utf8 strict/unsafe", utf::Char8, "\xF0\x9F\x96\xF0\x9F\x96\xA4", 3)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::toUnicodeUnsafe(outputCode, startIt));
		REQUIRE(startIt == invalidStr);
		REQUIRE(outputCode == 0);
	BWN_SECTION_END()
	
	BWN_SECTION_BEGIN("Utf8 relaxed/unsafe", utf::Char8, "\xF0\x9F\x96\xF0\x9F\x96\xA4", 3)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::toUnicodeUnsafe(outputCode, startIt));
		REQUIRE(startIt == invalidStrEnd);
		REQUIRE(outputCode == utf::detail::k_unicodeCharStub);
	BWN_SECTION_END()


	BWN_SECTION_BEGIN("Utf16 strict/safe", char16_t, u"\xD83D\xD83D\xDDA4", 1)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::toUnicodeSafe(outputCode, startIt, endIt));
		REQUIRE(startIt == invalidStr);
		REQUIRE(outputCode == 0);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 relaxed/safe", char16_t, u"\xD83D\xD83D\xDDA4", 1)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::toUnicodeSafe(outputCode, startIt, endIt));
		REQUIRE(startIt == invalidStrEnd);
		REQUIRE(outputCode == utf::detail::k_unicodeCharStub);
	BWN_SECTION_END()
	
	BWN_SECTION_BEGIN("Utf16 strict/unsafe", char16_t, u"\xD83D\xD83D\xDDA4", 1)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::toUnicodeUnsafe(outputCode, startIt));
		REQUIRE(startIt == invalidStr);
		REQUIRE(outputCode == 0);
	BWN_SECTION_END()
	
	BWN_SECTION_BEGIN("Utf16 relaxed/unsafe", char16_t, u"\xD83D\xD83D\xDDA4", 1)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::toUnicodeUnsafe(outputCode, startIt));
		REQUIRE(startIt == invalidStrEnd);
		REQUIRE(outputCode == utf::detail::k_unicodeCharStub);
	BWN_SECTION_END()

#undef BWN_SECTION_BEGIN
#undef BWN_SECTION_END
}

TEST_CASE("Valid Conversion from Unicode")
{
#define BWN_SECTION(p_name, p_charT, ...) \
	SECTION(p_name) \
	{ \
	using Char = p_charT; \
		const uint32_t tests[] \
		{ \
			0x00000061, \
			0x00000430, \
			0x00002661, \
			0x0001F5A4 \
		}; \
		const Char*const answers[] \
		{ \
			BWN_STRING_LITERAL(p_charT, "\U00000061"), \
			BWN_STRING_LITERAL(p_charT, "\U00000430"), \
			BWN_STRING_LITERAL(p_charT, "\U00002661"), \
			BWN_STRING_LITERAL(p_charT, "\U0001F5A4") \
		}; \
		Char tempBuff[8]; \
		const uint32_t* testIt = std::begin(tests); \
		const uint32_t* testEnd = std::end(tests); \
		const Char*const* answerIt = std::begin(answers); \
		const Char*const* answerEnd = std::end(answers); \
		for (; testIt != testEnd && answerIt != answerEnd; ++testIt, ++answerIt) \
		{ \
			Char* buffIt = std::begin(tempBuff); \
			REQUIRE(__VA_ARGS__); \
			const Char*const answerStrBeg = *answerIt; \
			const Char*const answerStrEnd = test::findStrTermination(answerStrBeg); \
			REQUIRE(test::compareBytes(answerStrBeg, answerStrEnd, std::begin(tempBuff), buffIt)); \
		} \
	}

	BWN_SECTION("Utf8 safe/strict", utf::Char8, utf::Algorithms<Char, utf::k_strict>::fromUnicodeSafe(buffIt, std::end(tempBuff), *testIt))
	BWN_SECTION("Utf8 safe/not strict", utf::Char8, utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeSafe(buffIt, std::end(tempBuff), *testIt))
	BWN_SECTION("Utf8 unsafe/strict", utf::Char8, utf::Algorithms<Char, utf::k_strict>::fromUnicodeUnsafe(buffIt, *testIt))
	BWN_SECTION("Utf8 unsafe/not strict", utf::Char8, utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeUnsafe(buffIt, *testIt))

	BWN_SECTION("Utf16 safe/strict", char16_t, utf::Algorithms<Char, utf::k_strict>::fromUnicodeSafe(buffIt, std::end(tempBuff), *testIt))
	BWN_SECTION("Utf16 safe/not strict", char16_t, utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeSafe(buffIt, std::end(tempBuff), *testIt))
	BWN_SECTION("Utf16 unsafe/strict", char16_t, utf::Algorithms<Char, utf::k_strict>::fromUnicodeUnsafe(buffIt, *testIt))
	BWN_SECTION("Utf16 unsafe/not strict", char16_t, utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeUnsafe(buffIt, *testIt))

	BWN_SECTION("Utf32 safe/strict", char32_t, utf::Algorithms<Char, utf::k_strict>::fromUnicodeSafe(buffIt, std::end(tempBuff), *testIt))
	BWN_SECTION("Utf32 safe/not strict", char32_t, utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeSafe(buffIt, std::end(tempBuff), *testIt))
	BWN_SECTION("Utf32 unsafe/strict", char32_t, utf::Algorithms<Char, utf::k_strict>::fromUnicodeUnsafe(buffIt, *testIt))
	BWN_SECTION("Utf32 unsafe/not strict", char32_t, utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeUnsafe(buffIt, *testIt))

	BWN_SECTION("Wchar safe/strict", wchar_t, utf::Algorithms<Char, utf::k_strict>::fromUnicodeSafe(buffIt, std::end(tempBuff), *testIt))
	BWN_SECTION("Wchar safe/not strict", wchar_t, utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeSafe(buffIt, std::end(tempBuff), *testIt))
	BWN_SECTION("Wchar unsafe/strict", wchar_t, utf::Algorithms<Char, utf::k_strict>::fromUnicodeUnsafe(buffIt, *testIt))
	BWN_SECTION("Wchar unsafe/not strict", wchar_t, utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeUnsafe(buffIt, *testIt))

#undef BWN_SECTION
}

TEST_CASE("Invalid Conversion from Unicode")
{

#define BWN_SECTION_BEGIN(p_name, p_charT) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		const uint32_t invalidUnicode = 0x0F000000; \
		const uint32_t longUnicode = 0x0001F5A4; \
		static constexpr size_t k_bufferSize = 8; \
		Char buffer[k_bufferSize] { 0 }; \
		Char originalBuffer[k_bufferSize] { 0 }; \
		Char stubBuffer[k_bufferSize] { 0 }; \
		Char* stubBufferValidEndIt = stubBuffer; \
		utf::Algorithms<Char>::fromUnicodeSafe(stubBufferValidEndIt, stubBuffer + k_bufferSize, utf::detail::k_unicodeCharStub); \
		Char*const bufferStartIt = buffer; \
		Char*const bufferEndIt = bufferStartIt + k_bufferSize; \
		Char*const invalidBufferEndIt = bufferStartIt + 1; \
		Char* startIt = bufferStartIt; \
		(void)invalidUnicode; \
		(void)longUnicode; \
		(void)originalBuffer; \
		(void)bufferEndIt; \
		(void)invalidBufferEndIt;

#define BWN_SECTION_END() }

	BWN_SECTION_BEGIN("Utf8 strict/safe/invalid iterator", utf::Char8)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::fromUnicodeSafe(startIt, invalidBufferEndIt, longUnicode));
		REQUIRE(startIt == bufferStartIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf8 relaxed/safe/invalid iterator", utf::Char8)
		REQUIRE(!utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeSafe(startIt, invalidBufferEndIt, longUnicode));
		REQUIRE(startIt == bufferStartIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf8 strict/safe/invalid unicode", utf::Char8)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::fromUnicodeSafe(startIt, bufferEndIt, invalidUnicode));
		REQUIRE(test::compareBytes(buffer, buffer + k_bufferSize, originalBuffer, originalBuffer + k_bufferSize));
		REQUIRE(startIt == bufferStartIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf8 relaxed/safe/invalid unicode", utf::Char8)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeSafe(startIt, bufferEndIt, invalidUnicode));
		REQUIRE(test::compareBytes(buffer, buffer + k_bufferSize, stubBuffer, stubBuffer + k_bufferSize));
		REQUIRE(std::distance(bufferStartIt, startIt) == std::distance(stubBuffer, stubBufferValidEndIt));
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf8 strict/unsafe/invalid unicode", utf::Char8)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::fromUnicodeUnsafe(startIt, invalidUnicode));
		REQUIRE(test::compareBytes(buffer, buffer + k_bufferSize, originalBuffer, originalBuffer + k_bufferSize));
		REQUIRE(startIt == bufferStartIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf8 relaxed/unsafe/invalid unicode", utf::Char8)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeUnsafe(startIt, invalidUnicode));
		REQUIRE(test::compareBytes(buffer, buffer + k_bufferSize, stubBuffer, stubBuffer + k_bufferSize));
		REQUIRE(std::distance(bufferStartIt, startIt) == std::distance(stubBuffer, stubBufferValidEndIt));
	BWN_SECTION_END()


	BWN_SECTION_BEGIN("Utf16 strict/safe/invalid iterator", char16_t)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::fromUnicodeSafe(startIt, invalidBufferEndIt, longUnicode));
		REQUIRE(startIt == bufferStartIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 relaxed/safe/invalid iterator", char16_t)
		REQUIRE(!utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeSafe(startIt, invalidBufferEndIt, longUnicode));
		REQUIRE(startIt == bufferStartIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 strict/safe/invalid unicode", char16_t)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::fromUnicodeSafe(startIt, bufferEndIt, invalidUnicode));
		REQUIRE(test::compareBytes(buffer, buffer + k_bufferSize, originalBuffer, originalBuffer + k_bufferSize));
		REQUIRE(startIt == bufferStartIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 relaxed/safe/invalid unicode", char16_t)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeSafe(startIt, bufferEndIt, invalidUnicode));
		REQUIRE(test::compareBytes(buffer, buffer + k_bufferSize, stubBuffer, stubBuffer + k_bufferSize));
		REQUIRE(std::distance(bufferStartIt, startIt) == std::distance(stubBuffer, stubBufferValidEndIt));
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 strict/unsafe/invalid unicode", char16_t)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::fromUnicodeUnsafe(startIt, invalidUnicode));
		REQUIRE(test::compareBytes(buffer, buffer + k_bufferSize, originalBuffer, originalBuffer + k_bufferSize));
		REQUIRE(startIt == bufferStartIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 relaxed/unsafe/invalid unicode", char16_t)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::fromUnicodeUnsafe(startIt, invalidUnicode));
		REQUIRE(test::compareBytes(buffer, buffer + k_bufferSize, stubBuffer, stubBuffer + k_bufferSize));
		REQUIRE(std::distance(bufferStartIt, startIt) == std::distance(stubBuffer, stubBufferValidEndIt));
	BWN_SECTION_END()

#undef BWN_SECTION_BEGIN
#undef BWN_SECTION_END
}

TEST_CASE("Valid Copy char")
{
#define BWN_SECTION(p_name, p_charT, ...) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		const Char*const strings[] \
		{ \
			BWN_STRING_LITERAL(Char, "\U00000061"), \
			BWN_STRING_LITERAL(Char, "\U00000430"), \
			BWN_STRING_LITERAL(Char, "\U00002661"), \
			BWN_STRING_LITERAL(Char, "\U0001F5A4") \
		}; \
		Char tempBuff[8]; \
		const Char*const* stringsIt = std::begin(strings); \
		const Char*const* stringsEnd = std::end(strings); \
		for (; stringsIt != stringsEnd; ++stringsIt) \
		{ \
			const Char*const strBeg = *stringsIt; \
			const Char*const strEnd = test::findStrTermination(strBeg); \
			Char*const buffBeg = std::begin(tempBuff); \
			Char*const buffEnd = std::end(tempBuff); \
			(void)buffEnd; \
			const Char* tempStrBeg = strBeg; \
			Char* tempBuffBeg = buffBeg; \
			REQUIRE(__VA_ARGS__); \
			REQUIRE(tempStrBeg == strEnd); \
			REQUIRE(test::compareBytes(strBeg, strEnd, buffBeg, tempBuffBeg)); \
		} \
	}

	BWN_SECTION("Utf8 safe/ranged/strict", utf::Char8, utf::Algorithms<Char, utf::k_strict>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg, strEnd))
	BWN_SECTION("Utf8 safe/ranged/not strict", utf::Char8, utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg, strEnd))
	BWN_SECTION("Utf8 safe/only start/strict", utf::Char8, utf::Algorithms<Char, utf::k_strict>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg))
	BWN_SECTION("Utf8 safe/only start/not strict", utf::Char8, utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg))
	BWN_SECTION("Utf8 unsafe/ranged/strict", utf::Char8, utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(tempBuffBeg, tempStrBeg, strEnd))
	BWN_SECTION("Utf8 unsafe/ranged/not strict", utf::Char8, utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(tempBuffBeg, tempStrBeg, strEnd))
	BWN_SECTION("Utf8 unsafe/only start/strict", utf::Char8, utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(tempBuffBeg, tempStrBeg))
	BWN_SECTION("Utf8 unsafe/only start/not strict", utf::Char8, utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(tempBuffBeg, tempStrBeg))

	BWN_SECTION("Utf16 safe/ranged/strict", char16_t, utf::Algorithms<Char, utf::k_strict>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg, strEnd))
	BWN_SECTION("Utf16 safe/ranged/not strict", char16_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg, strEnd))
	BWN_SECTION("Utf16 safe/only start/strict", char16_t, utf::Algorithms<Char, utf::k_strict>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg))
	BWN_SECTION("Utf16 safe/only start/not strict", char16_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg))
	BWN_SECTION("Utf16 unsafe/ranged/strict", char16_t, utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(tempBuffBeg, tempStrBeg, strEnd))
	BWN_SECTION("Utf16 unsafe/ranged/not strict", char16_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(tempBuffBeg, tempStrBeg, strEnd))
	BWN_SECTION("Utf16 unsafe/only start/strict", char16_t, utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(tempBuffBeg, tempStrBeg))
	BWN_SECTION("Utf16 unsafe/only start/not strict", char16_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(tempBuffBeg, tempStrBeg))

	BWN_SECTION("Utf32 safe/ranged/strict", char32_t, utf::Algorithms<Char, utf::k_strict>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg, strEnd))
	BWN_SECTION("Utf32 safe/ranged/not strict", char32_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg, strEnd))
	BWN_SECTION("Utf32 safe/only start/strict", char32_t, utf::Algorithms<Char, utf::k_strict>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg))
	BWN_SECTION("Utf32 safe/only start/not strict", char32_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg))
	BWN_SECTION("Utf32 unsafe/ranged/strict", char32_t, utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(tempBuffBeg, tempStrBeg, strEnd))
	BWN_SECTION("Utf32 unsafe/ranged/not strict", char32_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(tempBuffBeg, tempStrBeg, strEnd))
	BWN_SECTION("Utf32 unsafe/only start/strict", char32_t, utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(tempBuffBeg, tempStrBeg))
	BWN_SECTION("Utf32 unsafe/only start/not strict", char32_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(tempBuffBeg, tempStrBeg))

	BWN_SECTION("Wchar safe/ranged/strict", wchar_t, utf::Algorithms<Char, utf::k_strict>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg, strEnd))
	BWN_SECTION("Wchar safe/ranged/not strict", wchar_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg, strEnd))
	BWN_SECTION("Wchar safe/only start/strict", wchar_t, utf::Algorithms<Char, utf::k_strict>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg))
	BWN_SECTION("Wchar safe/only start/not strict", wchar_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(tempBuffBeg, buffEnd, tempStrBeg))
	BWN_SECTION("Wchar unsafe/ranged/strict", wchar_t, utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(tempBuffBeg, tempStrBeg, strEnd))
	BWN_SECTION("Wchar unsafe/ranged/not strict", wchar_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(tempBuffBeg, tempStrBeg, strEnd))
	BWN_SECTION("Wchar unsafe/only start/strict", wchar_t, utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(tempBuffBeg, tempStrBeg))
	BWN_SECTION("Wchar unsafe/only start/not strict", wchar_t, utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(tempBuffBeg, tempStrBeg))

#undef BWN_SECTION
}

TEST_CASE("Invalid Copy char")
{
#define BWN_INVALID_ITERATOR_SECTION(p_name, p_charT, ...) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		const Char*const strBeginIt = BWN_STRING_LITERAL(p_charT, "a"); \
		const Char*const strEndIt = strBeginIt + 1; \
		static constexpr size_t k_bufferCount = 8; \
		Char buffer[k_bufferCount]; \
		Char* bufferStartIt = buffer; \
		Char*const bufferEndIt = bufferStartIt + k_bufferCount; \
		Char*const invalidBufferEndIt = bufferStartIt - 4; \
		const Char* startIt = strBeginIt; \
		const Char*const invalidEndIt = startIt - 4; \
		(void)strEndIt; \
		(void)invalidBufferEndIt; \
		(void)invalidEndIt; \
		(void)bufferEndIt; \
		\
		REQUIRE(__VA_ARGS__); \
		REQUIRE(startIt == strBeginIt); \
		REQUIRE(bufferStartIt == buffer); \
	}

	BWN_INVALID_ITERATOR_SECTION("Utf8 strict/output clamped/same input", utf::Char8, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, bufferEndIt, startIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 strict/output unclamped/same input", utf::Char8, !utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(bufferStartIt, startIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 strict/output clamped/invalid input", utf::Char8, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, bufferEndIt, startIt, invalidEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 strict/output unclamped/invalid input", utf::Char8, !utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(bufferStartIt, startIt, invalidEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 strict/same output/clamped input", utf::Char8, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, bufferStartIt, startIt, strEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 strict/same output/unclamped input", utf::Char8, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, bufferStartIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 strict/invalid output/clamped input", utf::Char8, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, invalidBufferEndIt, startIt, strEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 strict/invalid output/unclamped input", utf::Char8, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, invalidBufferEndIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 relaxed/output clamped/same input", utf::Char8, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, bufferEndIt, startIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 relaxed/output unclamped/same input", utf::Char8, !utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(bufferStartIt, startIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 relaxed/output clamped/invalid input", utf::Char8, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, bufferEndIt, startIt, invalidEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 relaxed/output unclamped/invalid input", utf::Char8, !utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(bufferStartIt, startIt, invalidEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 relaxed/same output/clamped input", utf::Char8, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, bufferStartIt, startIt, strEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 relaxed/same output/unclamped input", utf::Char8, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, bufferStartIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 relaxed/invalid output/clamped input", utf::Char8, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, invalidBufferEndIt, startIt, strEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf8 relaxed/invalid output/unclamped input", utf::Char8, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, invalidBufferEndIt, startIt));

	BWN_INVALID_ITERATOR_SECTION("Utf16 strict/output clamped/same input", char16_t, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, bufferEndIt, startIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 strict/output unclamped/same input", char16_t, !utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(bufferStartIt, startIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 strict/output clamped/invalid input", char16_t, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, bufferEndIt, startIt, invalidEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 strict/output unclamped/invalid input", char16_t, !utf::Algorithms<Char, utf::k_strict>::copyCharUnsafe(bufferStartIt, startIt, invalidEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 strict/same output/clamped input", char16_t, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, bufferStartIt, startIt, strEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 strict/same output/unclamped input", char16_t, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, bufferStartIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 strict/invalid output/clamped input", char16_t, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, invalidBufferEndIt, startIt, strEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 strict/invalid output/unclamped input", char16_t, !utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferStartIt, invalidBufferEndIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 relaxed/output clamped/same input", char16_t, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, bufferEndIt, startIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 relaxed/output unclamped/same input", char16_t, !utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(bufferStartIt, startIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 relaxed/output clamped/invalid input", char16_t, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, bufferEndIt, startIt, invalidEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 relaxed/output unclamped/invalid input", char16_t, !utf::Algorithms<Char, utf::k_relaxed>::copyCharUnsafe(bufferStartIt, startIt, invalidEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 relaxed/same output/clamped input", char16_t, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, bufferStartIt, startIt, strEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 relaxed/same output/unclamped input", char16_t, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, bufferStartIt, startIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 relaxed/invalid output/clamped input", char16_t, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, invalidBufferEndIt, startIt, strEndIt));
	BWN_INVALID_ITERATOR_SECTION("Utf16 relaxed/invalid output/unclamped input", char16_t, !utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferStartIt, invalidBufferEndIt, startIt));

#undef BWN_INVALID_ITERATOR_SECTION


#define BWN_SECTION_BEGIN(p_name, p_charT, p_invalidString) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		const Char*const strBeginIt = p_invalidString; \
		const Char*const strEndIt = test::findStrTermination(strBeginIt); \
		static constexpr size_t k_bufferSize = 16; \
		Char buffer[k_bufferSize] { 0 }; \
		Char originalBuffer[k_bufferSize] { 0 }; \
		Char stubBuffer[k_bufferSize] { 0 }; \
		Char* stubBufferEndIt = stubBuffer; \
		utf::Algorithms<Char>::fromUnicodeSafe(stubBufferEndIt, stubBuffer + k_bufferSize, utf::detail::k_unicodeCharStub); \
		size_t stubStringSize = std::distance(stubBuffer, stubBufferEndIt); \
		Char* bufferIt = buffer; \
		const Char* stringIt = strBeginIt; \
		Char*const bufferEndIt = buffer + k_bufferSize; \
		(void)originalBuffer; \
		(void)stubStringSize;

#define BWN_SECTION_END() }

	BWN_SECTION_BEGIN("Utf8 strict/invalid char", utf::Char8, "\xF0\x9F\x96\xF0\x9F\x96\xA4")
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferIt, bufferEndIt, stringIt, strEndIt));
		REQUIRE(stringIt == strBeginIt);
		REQUIRE(bufferIt == buffer);
		REQUIRE(test::compareBytes(buffer, bufferEndIt, originalBuffer, originalBuffer + k_bufferSize));
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf8 relaxed/invalid char", utf::Char8, "\xF0\x9F\x96\xF0\x9F\x96\xA4")
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferIt, bufferEndIt, stringIt, strEndIt));
		REQUIRE(stringIt == strBeginIt + 3);
		REQUIRE(bufferIt == (buffer + stubStringSize));
		REQUIRE(test::compareBytes(buffer, bufferEndIt, stubBuffer, stubBuffer + k_bufferSize));
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 strict/invalid char", char16_t, u"\xD83D\xD83D\xDDA4")
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::copyCharSafe(bufferIt, bufferEndIt, stringIt, strEndIt));
		REQUIRE(stringIt == strBeginIt);
		REQUIRE(bufferIt == buffer);
		REQUIRE(test::compareBytes(buffer, bufferEndIt, originalBuffer, originalBuffer + k_bufferSize));
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 relaxed/invalid char", char16_t, u"\xD83D\xD83D\xDDA4")
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::copyCharSafe(bufferIt, bufferEndIt, stringIt, strEndIt));
		REQUIRE(stringIt == strBeginIt + 1);
		REQUIRE(bufferIt == (buffer + stubStringSize));
		REQUIRE(test::compareBytes(buffer, bufferEndIt, stubBuffer, stubBuffer + k_bufferSize));
	BWN_SECTION_END()

#undef BWN_SECTION_BEGIN
#undef BWN_SECTION_END
}

TEST_CASE("Valid Next")
{
#define BWN_SECTION(p_name, p_charT, ...) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		const Char*const strings[] \
		{ \
			BWN_STRING_LITERAL(p_charT, "\U00000061"), \
			BWN_STRING_LITERAL(p_charT, "\U00000430"), \
			BWN_STRING_LITERAL(p_charT, "\U00002661"), \
			BWN_STRING_LITERAL(p_charT, "\U0001F5A4") \
		}; \
		const Char*const* stringsIt = std::begin(strings); \
		const Char*const* stringsEnd = std::end(strings); \
		for (; stringsIt != stringsEnd; ++stringsIt) \
		{ \
			const Char*const strBeg = *stringsIt; \
			const Char*const charEnd = test::findStrTermination(strBeg); \
			const Char*const strEnd = charEnd + 1; \
			(void)strEnd; \
			const Char* tempStrBeg = strBeg; \
			REQUIRE(__VA_ARGS__); \
			REQUIRE(tempStrBeg == charEnd); \
		} \
	}

	BWN_SECTION("Utf8 safe/strict", utf::Char8, utf::Algorithms<Char, utf::k_strict>::nextSafe(tempStrBeg, strEnd))
	BWN_SECTION("Utf8 safe/not strict", utf::Char8, utf::Algorithms<Char, utf::k_relaxed>::nextSafe(tempStrBeg, strEnd))
	BWN_SECTION("Utf8 unsafe/strict", utf::Char8, utf::Algorithms<Char, utf::k_strict>::nextUnsafe(tempStrBeg))
	BWN_SECTION("Utf8 unafe/not strict", utf::Char8, utf::Algorithms<Char, utf::k_relaxed>::nextUnsafe(tempStrBeg))

	BWN_SECTION("Utf16 safe/strict", char16_t, utf::Algorithms<Char, utf::k_strict>::nextSafe(tempStrBeg, strEnd))
	BWN_SECTION("Utf16 safe/not strict", char16_t, utf::Algorithms<Char, utf::k_relaxed>::nextSafe(tempStrBeg, strEnd))
	BWN_SECTION("Utf16 unsafe/strict", char16_t, utf::Algorithms<Char, utf::k_strict>::nextUnsafe(tempStrBeg))
	BWN_SECTION("Utf16 unafe/not strict", char16_t, utf::Algorithms<Char, utf::k_relaxed>::nextUnsafe(tempStrBeg))

	BWN_SECTION("Utf32 safe/strict", char32_t, utf::Algorithms<Char, utf::k_strict>::nextSafe(tempStrBeg, strEnd))
	BWN_SECTION("Utf32 safe/not strict", char32_t, utf::Algorithms<Char, utf::k_relaxed>::nextSafe(tempStrBeg, strEnd))
	BWN_SECTION("Utf32 unsafe/strict", char32_t, utf::Algorithms<Char, utf::k_strict>::nextUnsafe(tempStrBeg))
	BWN_SECTION("Utf32 unafe/not strict", char32_t, utf::Algorithms<Char, utf::k_relaxed>::nextUnsafe(tempStrBeg))

	BWN_SECTION("Wchar safe/strict", wchar_t, utf::Algorithms<Char, utf::k_strict>::nextSafe(tempStrBeg, strEnd))
	BWN_SECTION("Wchar safe/not strict", wchar_t, utf::Algorithms<Char, utf::k_relaxed>::nextSafe(tempStrBeg, strEnd))
	BWN_SECTION("Wchar unsafe/strict", wchar_t, utf::Algorithms<Char, utf::k_strict>::nextUnsafe(tempStrBeg))
	BWN_SECTION("Wchar unafe/not strict", wchar_t, utf::Algorithms<Char, utf::k_relaxed>::nextUnsafe(tempStrBeg))

#undef BWN_SECTION
}

TEST_CASE("Invalid Next")
{
#define BWN_INVALID_ITERATOR_SECTION(p_name, p_charT, ...) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		Char localChar = 'a'; \
		const Char*const strBeginIt = &localChar; \
		const Char* startIt = strBeginIt; \
		REQUIRE(__VA_ARGS__); \
		REQUIRE(startIt == strBeginIt); \
	}

	BWN_INVALID_ITERATOR_SECTION("Utf8 strict/zero length", utf::Char8, !utf::Algorithms<Char, utf::k_strict>::nextSafe(startIt, strBeginIt))
	BWN_INVALID_ITERATOR_SECTION("Utf8 strict/negative length", utf::Char8, !utf::Algorithms<Char, utf::k_strict>::nextSafe(startIt, strBeginIt - 4))
	BWN_INVALID_ITERATOR_SECTION("Utf8 relaxed/zero length", utf::Char8, !utf::Algorithms<Char, utf::k_relaxed>::nextSafe(startIt, strBeginIt))
	BWN_INVALID_ITERATOR_SECTION("Utf8 relaxed/negative length", utf::Char8, !utf::Algorithms<Char, utf::k_relaxed>::nextSafe(startIt, strBeginIt - 4))

	BWN_INVALID_ITERATOR_SECTION("Utf16 strict/zero length", char16_t, !utf::Algorithms<Char, utf::k_strict>::nextSafe(startIt, strBeginIt))
	BWN_INVALID_ITERATOR_SECTION("Utf16 strict/negative length", char16_t, !utf::Algorithms<Char, utf::k_strict>::nextSafe(startIt, strBeginIt - 4))
	BWN_INVALID_ITERATOR_SECTION("Utf16 relaxed/zero length", char16_t, !utf::Algorithms<Char, utf::k_relaxed>::nextSafe(startIt, strBeginIt))
	BWN_INVALID_ITERATOR_SECTION("Utf16 relaxed/negative length", char16_t, !utf::Algorithms<Char, utf::k_relaxed>::nextSafe(startIt, strBeginIt - 4))

#undef BWN_INVALID_ITERATOR_SECTION


#define BWN_SECTION_BEGIN(p_name, p_charT, p_invalidString, p_invalidStringCount) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		const Char*const stringBeginIt = p_invalidString; \
		const Char*const invalidStringEndIt = stringBeginIt + p_invalidStringCount; \
		const Char*const stringEndIt = test::findStrTermination(stringBeginIt); \
		const Char* stringIt = stringBeginIt; \
		(void)invalidStringEndIt; \
		(void)stringEndIt;

#define BWN_SECTION_END() }

	BWN_SECTION_BEGIN("Utf8 strict/safe", utf::Char8, "\xF0\x9F\x96\xF0\x9F\x96\xA4", 3)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::nextSafe(stringIt, stringEndIt));
		REQUIRE(stringIt == stringBeginIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf8 strict/unsafe", utf::Char8, "\xF0\x9F\x96\xF0\x9F\x96\xA4", 3)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::nextUnsafe(stringIt));
		REQUIRE(stringIt == stringBeginIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf8 relaxed/safe", utf::Char8, "\xF0\x9F\x96\xF0\x9F\x96\xA4", 3)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::nextSafe(stringIt, stringEndIt));
		REQUIRE(stringIt == invalidStringEndIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf8 relaxed/unsafe", utf::Char8, "\xF0\x9F\x96\xF0\x9F\x96\xA4", 3)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::nextUnsafe(stringIt));
		REQUIRE(stringIt == invalidStringEndIt);
	BWN_SECTION_END()


	BWN_SECTION_BEGIN("Utf16 strict/safe", char16_t, u"\xD83D\xD83D\xDDA4", 1)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::nextSafe(stringIt, stringEndIt));
		REQUIRE(stringIt == stringBeginIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 strict/unsafe", char16_t, u"\xD83D\xD83D\xDDA4", 1)
		REQUIRE(!utf::Algorithms<Char, utf::k_strict>::nextUnsafe(stringIt));
		REQUIRE(stringIt == stringBeginIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 relaxed/safe", char16_t, u"\xD83D\xD83D\xDDA4", 1)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::nextSafe(stringIt, stringEndIt));
		REQUIRE(stringIt == invalidStringEndIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 relaxed/unsafe", char16_t, u"\xD83D\xD83D\xDDA4", 1)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::nextUnsafe(stringIt));
		REQUIRE(stringIt == invalidStringEndIt);
	BWN_SECTION_END()

#undef BWN_SECTION_BEGIN
#undef BWN_SECTION_END
}

TEST_CASE("Valid Distance")
{
#define BWN_SECTION(p_name, p_charT, ...) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		const Char*const tests[] \
		{ \
			BWN_STRING_LITERAL(p_charT, "\U00000061\U00000061\U00000061"), \
			BWN_STRING_LITERAL(p_charT, "\U00000061\U00000430\U00000430\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_charT, "\U00000061\U00000430\U00002661\U00002661\U00002661\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_charT, "\U00000061\U00000430\U00002661\U0001F5A4\U0001F5A4\U0001F5A4\U00002661\U00000430\U00000061") \
		}; \
		const size_t answers[] \
		{ \
			4, \
			6, \
			8, \
			10 \
		}; \
		const Char*const* testIt = std::begin(tests); \
		const Char*const*const testEnd = std::end(tests); \
		const size_t* answerIt = std::begin(answers); \
		const size_t*const answerEnd = std::end(answers); \
		for (; testIt != testEnd && answerIt != answerEnd; ++testIt, ++answerIt) \
		{ \
			const Char* strIt = *testIt; \
			const Char*const strEnd = test::findStrTermination(strIt) + 1; \
			REQUIRE(__VA_ARGS__ == *answerIt); \
			REQUIRE(strIt == strEnd); \
		} \
	}

	BWN_SECTION("Utf8", utf::Char8, utf::Algorithms<Char, utf::k_strict>::distance(strIt, strEnd))
	BWN_SECTION("Utf16", char16_t, utf::Algorithms<Char, utf::k_strict>::distance(strIt, strEnd))
	BWN_SECTION("Utf32", char32_t, utf::Algorithms<Char, utf::k_strict>::distance(strIt, strEnd))
	BWN_SECTION("Wchar", wchar_t, utf::Algorithms<Char, utf::k_strict>::distance(strIt, strEnd))

	BWN_SECTION("Utf8", utf::Char8, utf::Algorithms<Char, utf::k_relaxed>::distance(strIt, strEnd))
	BWN_SECTION("Utf16", char16_t, utf::Algorithms<Char, utf::k_relaxed>::distance(strIt, strEnd))
	BWN_SECTION("Utf32", char32_t, utf::Algorithms<Char, utf::k_relaxed>::distance(strIt, strEnd))
	BWN_SECTION("Wchar", wchar_t, utf::Algorithms<Char, utf::k_relaxed>::distance(strIt, strEnd))

#undef BWN_SECTION
}

TEST_CASE("Invalid Distance")
{

#define BWN_SECTION_BEGIN(p_name, p_charT, p_invalidString, p_invalidStringCount) \
	SECTION(p_name) \
	{ \
		using Char = p_charT; \
		const Char*const stringBeginIt = p_invalidString; \
		const Char*const invalidStringEndIt = stringBeginIt + p_invalidStringCount; \
		const Char*const stringEndIt = test::findStrTermination(stringBeginIt); \
		const Char* stringIt = stringBeginIt; \
		(void)invalidStringEndIt;

#define BWN_SECTION_END() }


	BWN_SECTION_BEGIN("Utf8 strict", utf::Char8, "\xF0\x9F\x96\xA4\xF0\x9F\x96\xF0\x9F\x96\xA4", 4)
		REQUIRE(utf::Algorithms<Char, utf::k_strict>::distance(stringIt, stringEndIt) == 1);
		REQUIRE(stringIt == invalidStringEndIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf8 relaxed", utf::Char8, "\xF0\x9F\x96\xA4\xF0\x9F\x96\xF0\x9F\x96\xA4", 4)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::distance(stringIt, stringEndIt) == 3);
		REQUIRE(stringIt == stringEndIt);
	BWN_SECTION_END()


	BWN_SECTION_BEGIN("Utf16 strict", char16_t, u"\xD83D\xDDA4\xD83D\xD83D\xDDA4", 2)
		REQUIRE(utf::Algorithms<Char, utf::k_strict>::distance(stringIt, stringEndIt) == 1);
		REQUIRE(stringIt == invalidStringEndIt);
	BWN_SECTION_END()

	BWN_SECTION_BEGIN("Utf16 relaxed", char16_t, u"\xD83D\xDDA4\xD83D\xD83D\xDDA4", 2)
		REQUIRE(utf::Algorithms<Char, utf::k_relaxed>::distance(stringIt, stringEndIt) == 3);
		REQUIRE(stringIt == stringEndIt);
	BWN_SECTION_END()

#undef BWN_SECTION_BEGIN
#undef BWN_SECTION_END
}

////////////////////////////////////////////////////////////////
///////////////////////// Converter checks ////////////////////////
////////////////////////////////////////////////////////////////

TEST_CASE("Single char Converters")
{

#define BWN_SECTION(p_name, p_outputCharT, p_inputCharT, ...) \
	SECTION(p_name) \
	{ \
		using OutputChar = p_outputCharT; \
		using InputChar = p_inputCharT; \
		const InputChar*const tests[] \
		{ \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000430"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U00002661"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U0001F5A4") \
		}; \
		const OutputChar*const answers[] \
		{ \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000430"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U00002661"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U0001F5A4") \
		}; \
		const InputChar*const* testIt = std::begin(tests); \
		const InputChar*const*const testEnd = std::end(tests); \
		const OutputChar*const* answerIt = std::begin(answers); \
		const OutputChar*const*const answerEnd = std::end(answers); \
		for (; testIt != testEnd && answerIt != answerEnd; ++testIt, ++answerIt) \
		{ \
			const InputChar* inputStrBeg = *testIt; \
			const InputChar*const inputStrEnd = test::findStrTermination(inputStrBeg); \
			(void)inputStrEnd; \
			const OutputChar*const answerStrBeg = *answerIt; \
			const OutputChar*const answerStrEnd = test::findStrTermination(answerStrBeg); \
			OutputChar outputBuff[8] {0}; \
			OutputChar*const outputStrBeg = std::begin(outputBuff); \
			OutputChar*const outputStrEnd = std::end(outputBuff); \
			OutputChar* tempOutputStrBeg = outputStrBeg; \
			REQUIRE(__VA_ARGS__); \
			OutputChar*const tempOutputStrEnd = test::findStrTermination(outputStrBeg); \
			REQUIRE(tempOutputStrEnd < outputStrEnd); \
			REQUIRE(tempOutputStrBeg > outputStrBeg); \
			REQUIRE(test::compareUtf(answerStrBeg, answerStrEnd, outputStrBeg, tempOutputStrBeg)); \
		} \
	}

	BWN_SECTION("Utf8 to Utf8 Safe/Safe", utf::Char8, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Utf8 Unsafe/Safe", utf::Char8, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Utf8 Safe/Unsafe", utf::Char8, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf8 to Utf8 Unsafe/Unsafe", utf::Char8, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Utf16 to Utf8 Safe/Safe", char16_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf8 Unsafe/Safe", char16_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf8 Safe/Unsafe", char16_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf16 to Utf8 Unsafe/Unsafe", char16_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Utf32 to Utf8 Safe/Safe", char32_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf8 Unsafe/Safe", char32_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf8 Safe/Unsafe", char32_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf32 to Utf8 Unsafe/Unsafe", char32_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Wchar to Utf8 Safe/Safe", wchar_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf8 Unsafe/Safe", wchar_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf8 Safe/Unsafe", wchar_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Wchar to Utf8 Unsafe/Unsafe", wchar_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))


	BWN_SECTION("Utf8 to Utf16 Safe/Safe", utf::Char8, char16_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Utf16 Unsafe/Safe", utf::Char8, char16_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Utf16 Safe/Unsafe", utf::Char8, char16_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf8 to Utf16 Unsafe/Unsafe", utf::Char8, char16_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Utf16 to Utf16 Safe/Safe", char16_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf16 Unsafe/Safe", char16_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf16 Safe/Unsafe", char16_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf16 to Utf16 Unsafe/Unsafe", char16_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Utf32 to Utf16 Safe/Safe", char32_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf16 Unsafe/Safe", char32_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf16 Safe/Unsafe", char32_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf32 to Utf16 Unsafe/Unsafe", char32_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Wchar to Utf16 Safe/Safe", wchar_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf16 Unsafe/Safe", wchar_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf16 Safe/Unsafe", wchar_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Wchar to Utf16 Unsafe/Unsafe", wchar_t, char16_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))


	BWN_SECTION("Utf8 to Utf32 Safe/Safe", utf::Char8, char32_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Utf32 Unsafe/Safe", utf::Char8, char32_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Utf32 Safe/Unsafe", utf::Char8, char32_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf8 to Utf32 Unsafe/Unsafe", utf::Char8, char32_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Utf16 to Utf32 Safe/Safe", char16_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf32 Unsafe/Safe", char16_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf32 Safe/Unsafe", char16_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf16 to Utf32 Unsafe/Unsafe", char16_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Utf32 to Utf32 Safe/Safe", char32_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf32 Unsafe/Safe", char32_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf32 Safe/Unsafe", char32_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf32 to Utf32 Unsafe/Unsafe", char32_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Wchar to Utf32 Safe/Safe", wchar_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf32 Unsafe/Safe", wchar_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf32 Safe/Unsafe", wchar_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Wchar to Utf32 Unsafe/Unsafe", wchar_t, char32_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))


	BWN_SECTION("Utf8 to Wchar Safe/Safe", utf::Char8, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Wchar Unsafe/Safe", utf::Char8, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Wchar Safe/Unsafe", utf::Char8, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf8 to Wchar Unsafe/Unsafe", utf::Char8, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Utf16 to Wchar Safe/Safe", char16_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Wchar Unsafe/Safe", char16_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Wchar Safe/Unsafe", char16_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf16 to Wchar Unsafe/Unsafe", char16_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Utf32 to Wchar Safe/Safe", char32_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Wchar Unsafe/Safe", char32_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Wchar Safe/Unsafe", char32_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Utf32 to Wchar Unsafe/Unsafe", char32_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

	BWN_SECTION("Wchar to Wchar Safe/Safe", wchar_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Wchar Unsafe/Safe", wchar_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Wchar Safe/Unsafe", wchar_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg))
	BWN_SECTION("Wchar to Wchar Unsafe/Unsafe", wchar_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertCharUnsafe(tempOutputStrBeg, inputStrBeg))

#undef BWN_SECTION
}

TEST_CASE("String Converters")
{

#define BWN_SECTION(p_name, p_outputCharT, p_inputCharT, ...) \
	SECTION(p_name) \
	{ \
		using OutputChar = p_outputCharT; \
		using InputChar = p_inputCharT; \
		const InputChar*const tests[] \
		{ \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000061\U00000061"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00000430\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00002661\U00002661\U00002661\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00002661\U0001F5A4\U0001F5A4\U0001F5A4\U00002661\U00000430\U00000061") \
		}; \
		const OutputChar*const answers[] \
		{ \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000061\U00000061"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000430\U00000430\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000430\U00002661\U00002661\U00002661\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000430\U00002661\U0001F5A4\U0001F5A4\U0001F5A4\U00002661\U00000430\U00000061") \
		}; \
		const InputChar*const* testIt = std::begin(tests); \
		const InputChar*const*const testEnd = std::end(tests); \
		const OutputChar*const* answerIt = std::begin(answers); \
		const OutputChar*const*const answerEnd = std::end(answers); \
		for (; testIt != testEnd && answerIt != answerEnd; ++testIt, ++answerIt) \
		{ \
			const InputChar* inputStrBeg = *testIt; \
			const InputChar*const inputStrEnd = test::findStrTermination(inputStrBeg) + 1; \
			const OutputChar*const answerStrBeg = *answerIt; \
			const OutputChar*const answerStrEnd = test::findStrTermination(answerStrBeg) + 1; \
			OutputChar outputBuff[128]; \
			OutputChar*const outputStrBeg = std::begin(outputBuff); \
			OutputChar*const outputStrEnd = std::end(outputBuff); \
			OutputChar* tempOutputStrBeg = outputStrBeg; \
			REQUIRE(__VA_ARGS__); \
			OutputChar*const tempOutputStrEnd = test::findStrTermination(outputStrBeg) + 1; \
			REQUIRE(tempOutputStrEnd < outputStrEnd); \
			REQUIRE(tempOutputStrBeg > outputStrBeg); \
			REQUIRE(test::compareUtf(answerStrBeg, answerStrEnd, outputStrBeg, tempOutputStrBeg)); \
		} \
	}

	BWN_SECTION("Utf8 to Utf8 Safe", utf::Char8, utf::Char8, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Utf8 Unsafe", utf::Char8, utf::Char8, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf8 Safe", char16_t, utf::Char8, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf8 Unsafe", char16_t, utf::Char8, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf8 Safe", char32_t, utf::Char8, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf8 Unsafe", char32_t, utf::Char8, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf8 Safe", wchar_t, utf::Char8, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf8 Unsafe", wchar_t, utf::Char8, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))


	BWN_SECTION("Utf8 to Utf16 Safe", utf::Char8, char16_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Utf16 Unsafe", utf::Char8, char16_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf16 Safe", char16_t, char16_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf16 Unsafe", char16_t, char16_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf16 Safe", char32_t, char16_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf16 Unsafe", char32_t, char16_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf16 Safe", wchar_t, char16_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf16 Unsafe", wchar_t, char16_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))


	BWN_SECTION("Utf8 to Utf32 Safe", utf::Char8, char32_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Utf32 Unsafe", utf::Char8, char32_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf32 Safe", char16_t, char32_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf32 Unsafe", char16_t, char32_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf32 Safe", char32_t, char32_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf32 Unsafe", char32_t, char32_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf32 Safe", wchar_t, char32_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf32 Unsafe", wchar_t, char32_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))


	BWN_SECTION("Utf8 to Wchar Safe", utf::Char8, wchar_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf8 to Wchar Unsafe", utf::Char8, wchar_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Wchar Safe", char16_t, wchar_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Wchar Unsafe", char16_t, wchar_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Wchar Safe", char32_t, wchar_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Wchar Unsafe", char32_t, wchar_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Wchar Safe", wchar_t, wchar_t, utf::Converter<OutputChar, InputChar>::covertStringSafe(tempOutputStrBeg, outputStrEnd, inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Wchar Unsafe", wchar_t, wchar_t, utf::Converter<OutputChar, InputChar>::covertStringUnsafe(tempOutputStrBeg, inputStrBeg, inputStrEnd))

#undef BWN_SECTION
}

TEST_CASE("String Container Converters from Iterators")
{

#define BWN_SECTION(p_name, p_outputCharT, p_inputCharT, ...) \
	SECTION(p_name) \
	{ \
		using OutputChar = p_outputCharT; \
		using InputChar = p_inputCharT; \
		using String = std::basic_string<OutputChar>; \
		const InputChar*const tests[] \
		{ \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000061\U00000061"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00000430\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00002661\U00002661\U00002661\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00002661\U0001F5A4\U0001F5A4\U0001F5A4\U00002661\U00000430\U00000061") \
		}; \
		const OutputChar*const answers[] \
		{ \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000061\U00000061"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000430\U00000430\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000430\U00002661\U00002661\U00002661\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000430\U00002661\U0001F5A4\U0001F5A4\U0001F5A4\U00002661\U00000430\U00000061") \
		}; \
		const InputChar*const* testIt = std::begin(tests); \
		const InputChar*const*const testEnd = std::end(tests); \
		const OutputChar*const* answerIt = std::begin(answers); \
		const OutputChar*const*const answerEnd = std::end(answers); \
		for (; testIt != testEnd && answerIt != answerEnd; ++testIt, ++answerIt) \
		{ \
			const InputChar* inputStrBeg = *testIt; \
			const InputChar*const inputStrEnd = test::findStrTermination(inputStrBeg); \
			const OutputChar*const answerStrBeg = *answerIt; \
			const OutputChar*const answerStrEnd = test::findStrTermination(answerStrBeg); \
			const String tempString = __VA_ARGS__; \
			const OutputChar*const outputStrBeg = tempString.data(); \
			const OutputChar*const outputStrEnd = outputStrBeg + tempString.size(); \
			REQUIRE(test::compareUtf(answerStrBeg, answerStrEnd, outputStrBeg, outputStrEnd)); \
		} \
	}

	BWN_SECTION("Utf8 to Utf8 string", utf::Char8, utf::Char8, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf8 string", char16_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf8 string", char32_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf8 string", wchar_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))

	BWN_SECTION("Utf8 to Utf16 string", utf::Char8, char16_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf16 string", char16_t, char16_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf16 string", char32_t, char16_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf16 string", wchar_t, char16_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))

	BWN_SECTION("Utf8 to Utf32 string", utf::Char8, char32_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Utf32 string", char16_t, char32_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Utf32 string", char32_t, char32_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Utf32 string", wchar_t, char32_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))

	BWN_SECTION("Utf8 to Wchar string", utf::Char8, wchar_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf16 to Wchar string", char16_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Utf32 to Wchar string", char32_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))
	BWN_SECTION("Wchar to Wchar string", wchar_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertToContainer<String>(inputStrBeg, inputStrEnd))

#undef BWN_SECTION
}

TEST_CASE("String Container Converters from another Container")
{

#define BWN_SECTION(p_name, p_outputCharT, p_inputCharT, ...) \
	SECTION(p_name) \
	{ \
		using InputChar = p_inputCharT; \
		using OutputChar = p_outputCharT; \
		using InputString = std::basic_string<InputChar>; \
		using OutputString = std::basic_string<OutputChar>; \
		const InputString tests[] \
		{ \
			InputString(BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000061\U00000061")), \
			InputString(BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00000430\U00000430\U00000061")), \
			InputString(BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00002661\U00002661\U00002661\U00000430\U00000061")), \
			InputString(BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00002661\U0001F5A4\U0001F5A4\U0001F5A4\U00002661\U00000430\U00000061")) \
		}; \
		const OutputChar*const answers[] \
		{ \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000061\U00000061"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000430\U00000430\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000430\U00002661\U00002661\U00002661\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_outputCharT, "\U00000061\U00000430\U00002661\U0001F5A4\U0001F5A4\U0001F5A4\U00002661\U00000430\U00000061") \
		}; \
		const InputString* testIt = std::begin(tests); \
		const InputString*const testEnd = std::end(tests); \
		const OutputChar*const* answerIt = std::begin(answers); \
		const OutputChar*const*const answerEnd = std::end(answers); \
		for (; testIt != testEnd && answerIt != answerEnd; ++testIt, ++answerIt) \
		{ \
			const InputString& testString = *testIt; \
			const OutputChar*const answerStrBeg = *answerIt; \
			const OutputChar*const answerStrEnd = test::findStrTermination(answerStrBeg); \
			const OutputString tempString = __VA_ARGS__; \
			const OutputChar*const outputStrBeg = tempString.data(); \
			const OutputChar*const outputStrEnd = outputStrBeg + tempString.size(); \
			REQUIRE(test::compareUtf(answerStrBeg, answerStrEnd, outputStrBeg, outputStrEnd)); \
		} \
	}

	BWN_SECTION("Utf8 to Utf8 string", utf::Char8, utf::Char8, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Utf16 to Utf8 string", char16_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Utf32 to Utf8 string", char32_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Wchar to Utf8 string", wchar_t, utf::Char8, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))

	BWN_SECTION("Utf8 to Utf16 string", utf::Char8, char16_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Utf16 to Utf16 string", char16_t, char16_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Utf32 to Utf16 string", char32_t, char16_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Wchar to Utf16 string", wchar_t, char16_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))

	BWN_SECTION("Utf8 to Utf32 string", utf::Char8, char32_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Utf16 to Utf32 string", char16_t, char32_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Utf32 to Utf32 string", char32_t, char32_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Wchar to Utf32 string", wchar_t, char32_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))

	BWN_SECTION("Utf8 to Wchar string", utf::Char8, wchar_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Utf16 to Wchar string", char16_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Utf32 to Wchar string", char32_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))
	BWN_SECTION("Wchar to Wchar string", wchar_t, wchar_t, utf::Converter<OutputChar, InputChar>::convertToContainer<OutputString>(testString))

#undef BWN_SECTION
}

template<typename CharT, utf::EStrictness k_strictness>
bool testMethod_IterativeConverter(const CharT*const p_inputBegin, const CharT*const p_inputEnd, const char32_t*const p_answerBegin, const char32_t*const p_answerEnd)
{
	std::basic_string<char32_t> outputString;
	utf::IterativeConverter<CharT, k_strictness> converter;
	for (const CharT* charIt = p_inputBegin; charIt != p_inputEnd; ++charIt)
	{
		char32_t result;
		if (converter.push(*charIt, result))
		{
			outputString.push_back(result);
		}
	}
	const char32_t*const outputStrBeg = outputString.data();
	const char32_t*const outputStrEnd = outputStrBeg + outputString.size();
	return test::compareUtf(p_answerBegin, p_answerEnd, outputStrBeg, outputStrEnd);
}

TEST_CASE("Valid String converted by IterativeConverter")
{
#define BWN_SECTION(p_name, p_inputCharT, p_strictness) \
	SECTION(p_name) \
	{ \
		using InputChar = p_inputCharT; \
		const InputChar*const tests[] \
		{ \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000061\U00000061"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00000430\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00002661\U00002661\U00002661\U00000430\U00000061"), \
			BWN_STRING_LITERAL(p_inputCharT, "\U00000061\U00000430\U00002661\U0001F5A4\U0001F5A4\U0001F5A4\U00002661\U00000430\U00000061") \
		}; \
		const char32_t*const answers[] \
		{ \
			U"\U00000061\U00000061\U00000061", \
			U"\U00000061\U00000430\U00000430\U00000430\U00000061", \
			U"\U00000061\U00000430\U00002661\U00002661\U00002661\U00000430\U00000061", \
			U"\U00000061\U00000430\U00002661\U0001F5A4\U0001F5A4\U0001F5A4\U00002661\U00000430\U00000061" \
		}; \
		const InputChar*const* testIt = std::begin(tests); \
		const InputChar*const*const testEnd = std::end(tests); \
		const char32_t*const* answerIt = std::begin(answers); \
		const char32_t*const*const answerEnd = std::end(answers); \
		for (; testIt != testEnd && answerIt != answerEnd; ++testIt, ++answerIt) \
		{ \
			const InputChar*const testStrBeg = *testIt; \
			const InputChar*const testStrEnd = test::findStrTermination(testStrBeg); \
			const char32_t*const answerStrBeg = *answerIt; \
			const char32_t*const answerStrEnd = test::findStrTermination(answerStrBeg); \
			REQUIRE(testMethod_IterativeConverter<InputChar, p_strictness>(testStrBeg, testStrEnd, answerStrBeg, answerStrEnd)); \
		} \
	}

	BWN_SECTION("Utf8 strict", utf::Char8, utf::EStrictness::k_strict)
	BWN_SECTION("Utf16 strict", char16_t, utf::EStrictness::k_strict)
	BWN_SECTION("Utf32 strict", char32_t, utf::EStrictness::k_strict)
	BWN_SECTION("Wchar strict", wchar_t, utf::EStrictness::k_strict)
	BWN_SECTION("Utf8 relaxed", utf::Char8, utf::EStrictness::k_relaxed)
	BWN_SECTION("Utf16 relaxed", char16_t, utf::EStrictness::k_relaxed)
	BWN_SECTION("Utf32 relaxed", char32_t, utf::EStrictness::k_relaxed)
	BWN_SECTION("Wchar relaxed", wchar_t, utf::EStrictness::k_relaxed)

#undef BWN_SECTION
}

TEST_CASE("Invalid String converted by IterativeConverter")
{
	// \U0001F5A4 -> \xF0\x9F\x96\xA4 -> \xD83D \xDDA4
	// \U00002661 -> \xE2\x99\xA1 
	// \U00000430 -> \xD0\xB0
	// utf::detail::k_unicodeCharStub (\U0000FFFD) -> \xEF\xBF\xBD -> \xFFFD

#define BWN_SECTION(p_name, p_inputCharT, p_strictness, p_testsArray, p_answersArray) \
	SECTION(p_name) \
	{ \
		using InputChar = p_inputCharT; \
		const InputChar*const* testIt = std::begin(p_testsArray); \
		const InputChar*const*const testEnd = std::end(p_testsArray); \
		const char32_t*const* answerIt = std::begin(p_answersArray); \
		const char32_t*const*const answerEnd = std::end(p_answersArray); \
		for (; testIt != testEnd && answerIt != answerEnd; ++testIt, ++answerIt) \
		{ \
			const InputChar*const testStrBeg = *testIt; \
			const InputChar*const testStrEnd = test::findStrTermination(testStrBeg); \
			const char32_t*const answerStrBeg = *answerIt; \
			const char32_t*const answerStrEnd = test::findStrTermination(answerStrBeg); \
			REQUIRE(testMethod_IterativeConverter<InputChar, p_strictness>(testStrBeg, testStrEnd, answerStrBeg, answerStrEnd)); \
		} \
	}

	const utf::Char8*const utf8Tests[]
	{
		u8"\xF0_\xF0\x9F_\xF0\x9F\x96_\x9F\x96\xA4\xF0\x9F\x96\xA4_\x9F\xF0\x9F\x96\xA4_\xF0\x9F\x96",
		u8"\xF0\xF0\x9F\xF0\x9F\x96\xF0\x9F\x96\xA4"
	};
	const char32_t*const utf8AnswersStrict[]
	{
		U"___\U0001F5A4_\U0001F5A4_",
		U"\U0001F5A4"
	};
	const char32_t*const utf8AnswersRelaxed[]
	{
		U"___\U0000FFFD\U0000FFFD\U0000FFFD\U0001F5A4_\U0000FFFD\U0001F5A4_",
		U"\U0000FFFD\U0000FFFD\U0000FFFD\U0001F5A4"
	};

	const char16_t*const utf16Tests[]
	{
		u"\xD83D_\xD83D_\xDDA4_\xD83D\xDDA4\xDDA4\xD83D",
		u"\xD83D\xD83D\xD83D"
	};
	const char32_t*const utf16AnswersStrict[]
	{
		U"___\U0001F5A4",
		U""
	};
	const char32_t*const utf16AnswersRelaxed[]
	{
		U"__\U0000FFFD_\U0001F5A4\U0000FFFD",
		U"\U0000FFFD\U0000FFFD"
	};

	BWN_SECTION("Utf8 strict", utf::Char8, utf::EStrictness::k_strict, utf8Tests, utf8AnswersStrict)
	BWN_SECTION("Utf8 relaxed", utf::Char8, utf::EStrictness::k_relaxed, utf8Tests, utf8AnswersRelaxed)

	BWN_SECTION("Utf16 strict", char16_t, utf::EStrictness::k_strict, utf16Tests, utf16AnswersStrict)
	BWN_SECTION("Utf16 relaxed", char16_t, utf::EStrictness::k_relaxed, utf16Tests, utf16AnswersRelaxed)

#undef BWN_SECTION
}